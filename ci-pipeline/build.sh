#!/bin/sh

npm i --no-audit --silent
npm i --no-audit --silent --prefix client

npm run build:client
npm run build:prod


if [ ! -d "dist" ]; then
  echo "ERROR: dist folder was not created"
  exit 1
fi

if [ ! -d "client/www" ]; then
  echo "ERROR: client/www folder was not created"
  exit 1
fi

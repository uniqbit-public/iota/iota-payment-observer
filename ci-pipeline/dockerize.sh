#!/bin/sh

# correct for actual prod build tag (e. g. v0.0.0)
IMAGE_TAG=$CI_PIPELINE_ID-STAGING
if [ ! -z "$CI_BUILD_TAG" ]; then
  IMAGE_TAG=$CI_BUILD_TAG
fi


# create the docker login config for kaniko
echo "{\"auths\":{\"$CI_REGISTRY\":{\"username\":\"$CI_REGISTRY_USER\",\"password\":\"$CI_REGISTRY_PASSWORD\"}}}" | tr -d '\n' > /kaniko/.docker/config.json
# echo "{\"auths\":{\"$DOCKER_REGISTRY\":{\"username\":\"$DOCKER_USER\",\"password\":\"$DOCKER_ACCESS_TOKEN\"}}}" | tr -d '\n' > /kaniko/.docker/config.json


if [ "$ENV" = "PROD" ]; then
  # /kaniko/executor  --context . \
  #                   --dockerfile Dockerfile \
  #                   --destination "$DOCKER_REGISTRY/$DOCKER_USER/$DOCKER_REPO:$IMAGE_TAG" \
  #                   --destination "$DOCKER_REGISTRY/$DOCKER_USER/$DOCKER_REPO:latest"
  /kaniko/executor  --context . \
                    --dockerfile Dockerfile \
                    --destination "$CI_REGISTRY/$CI_PROJECT_PATH/$IMAGE_NAME:$IMAGE_TAG" \
                    --destination "$CI_REGISTRY/$CI_PROJECT_PATH/$IMAGE_NAME:latest"
  echo "pushed image: $CI_REGISTRY/$CI_PROJECT_PATH/$IMAGE_NAME:$IMAGE_TAG"
  echo "pushed image: $CI_REGISTRY/$CI_PROJECT_PATH/$IMAGE_NAME:latest"
else
  # /kaniko/executor  --context . \
  #                   --dockerfile Dockerfile \
  #                   --destination "$DOCKER_REGISTRY/$DOCKER_USER/$DOCKER_REPO:latest-dev"
  /kaniko/executor  --context . \
                    --dockerfile Dockerfile \
                    --destination "$CI_REGISTRY/$CI_PROJECT_PATH/$IMAGE_NAME:latest-dev"
  echo "pushed image: $CI_REGISTRY/$CI_PROJECT_PATH/$IMAGE_NAME:latest-dev"
fi


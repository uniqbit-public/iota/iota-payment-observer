#!/bin/sh

DOCKER_IMAGE=registry.gitlab.com/uniqbit-public/iota/iota-payment-observer/iota-payment-observer:latest-dev


docker rm -f ipo 
docker pull $DOCKER_IMAGE


docker run  --name ipo -p 4040:4040 \
            --network=ipo_net \
            -e POSTGRES_PASSWORD=development \
            -e SEED_ENCRYPTION_PW=development \
            -e COLLECTOR_ADDRESS=atoi1qr6rzfshdvf383wvag2jg8tmk6lgnmsfh7telf4exr73pv3edsllkjkcq7y \
            -e TX_START_WEBHOOK="http://localhost:4040/ipo/v1/tx/start" \
            -e TX_COMPLETE_WEBHOOK="http://localhost:4040/ipo/v1/tx/success" \
            -e TX_CANCELED_WEBHOOK="http://localhost:4040/ipo/v1/tx/cancel" \
            -e TX_PARTIAL_WEBHOOK="http://localhost:4040/ipo/v1/tx/part" \
            -d $DOCKER_IMAGE


docker logs -f ipo

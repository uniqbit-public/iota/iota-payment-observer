import { NestFactory } from '@nestjs/core';
import { Logger } from '@nestjs/common';
import { AppModule } from './app.module';
import { CookieParserService } from './providers/services/cookie-parser.service';
import { json, urlencoded } from 'body-parser';
import { CleanerGuard } from './providers/guards';

const logger = new Logger('main.ts');

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  const cookieParser = new CookieParserService();
  const sizeLimit = '20mb';
  const port = process.env.APP_PORT || 4040;

  app.enableCors({
    origin: (origin, callback) => {
      return callback(undefined, true);
    },
    allowedHeaders: 'Authorization,Content-Type,Accept,command_csrf',
    credentials: true,
    preflightContinue: false,
    methods: 'GET,PATCH,PUT,POST,DELETE,OPTIONS',
    optionsSuccessStatus: 204,
  });

  app.useGlobalGuards(new CleanerGuard());
  app.use((req: any, res: any, next: any) => {
    cookieParser.parse(req, res);
    next();
  });
  app.setGlobalPrefix('ipo/:v');

  app.use(json({ limit: sizeLimit }));
  app.use(urlencoded({ limit: sizeLimit, extended: true }));

  await app.listen(port);
  logger.debug('ipo is listening on port ' + port);
}
bootstrap();

import { Module, Logger } from '@nestjs/common';
import { ServeStaticModule } from '@nestjs/serve-static';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ConfigModule } from '@nestjs/config';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { GlobalProvidersModule } from './providers/global-providers.module';
import {
  GlobalConfigService,
  envVars,
} from './providers/services/config.service';
import { LoggingService } from './providers/services/logging.service';
import { HealthCheckModule } from './routes/healthcheck/healthcheck.module';
import { AddressModule } from './routes/address/address.module';
import { LegalModule } from './routes/legal/legal.module';
import { PriceModule } from './routes/price/price.module';
import { TestModule } from './routes/test/test.module';
import { RecoveryModule } from './routes/recovery/recovery.module';
import * as path from 'path';

const MODULES = [
  GlobalProvidersModule,
  TypeOrmModule.forRootAsync({
    imports: [ConfigModule, GlobalProvidersModule],
    inject: [GlobalConfigService],
    useFactory: async (configService: GlobalConfigService) => ({
      type: 'postgres',
      host: configService.getValue('POSTGRES_HOST', true),
      port: parseInt(configService.getValue('POSTGRES_PORT', true)),
      username: configService.getValue('POSTGRES_USER', true),
      password: configService.getValue('POSTGRES_PASSWORD', true),
      database: configService.getValue('POSTGRES_DB', true),
      entities: [__dirname + '/**/*.entity{.ts,.js}'],
      synchronize: true,
    }),
  }),
  ConfigModule.forRoot({
    isGlobal: true,
    envFilePath:
      '.env.' +
      String(
        String(process.env.FORCE_DEV_VARS).trim() === '1' ? 'DEV' : envVars.ENV
      )
        .trim()
        .toLowerCase(),
  }),
  ServeStaticModule.forRoot({
    rootPath:
      String(envVars.ENV).trim() === 'DEV'
        ? path.join(process.cwd(), 'client/src')
        : path.join(process.cwd(), 'client/www'),
    exclude: ['/ipo*'],
    serveRoot: '/ipo',
    serveStaticOptions: {
      cacheControl: true,
      extensions: ['svg', 'jpg', 'jpeg', 'git', 'png'],
      maxAge: '365d',
    },
  }),
  HealthCheckModule,
  AddressModule,
  LegalModule,
  PriceModule,
  TestModule,
  RecoveryModule,
];

const SERVICES = [Logger, LoggingService, AppService];

@Module({
  imports: [...MODULES],
  controllers: [AppController],
  providers: [...SERVICES],
})
export class AppModule {}

import { Controller, Get, Res, Render } from '@nestjs/common';
import {
  GlobalConfigService,
  envVars,
} from './providers/services/config.service';
import * as path from 'path';
import { AppService } from './app.service';
import { ApiVersion } from './providers/guards';
import { Response } from 'express';

@Controller()
export class AppController {
  constructor(
    private readonly appService: AppService,
    private readonly configService: GlobalConfigService
  ) {
    // const staticServerPath =
    //   String(envVars.ENV).trim() === 'DEV'
    //     ? path.join(process.cwd(), 'client/src')
    //     : path.join(process.cwd(), 'client/www');
    // const dbSettings = {
    //   type: 'postgres',
    //   host: configService.getValue('POSTGRES_HOST', true),
    //   port: parseInt(configService.getValue('POSTGRES_PORT', true)),
    //   username: configService.getValue('POSTGRES_USER', true),
    //   password: configService.getValue('POSTGRES_PASSWORD', true),
    //   database: configService.getValue('POSTGRES_DB', true),
    //   entities: [__dirname + '/**/*.entity{.ts,.js}'],
    //   synchronize: true,
    // };
    // // TESTING
    // console.info({ env: envVars.ENV, staticServerPath, dbSettings });
  }

  // DO NOT REGISTER ANY ROUTES IN HERE, OR STATIC index.html SERVING WILL NO LONGER WORK!

  // @Get()
  // @Render('index')
  // root() {}
  // getIndex(@Res() res: Response) {
  //   this.appService.getIndex(res);
  // }
}

import { Entity, PrimaryGeneratedColumn, BeforeInsert, BeforeUpdate, Column } from 'typeorm';

@Entity()
export class HealthCheck {
  @PrimaryGeneratedColumn('uuid')
  healthid: string;

  @Column({ nullable: true })
  createdAt: Date;

  @Column({ nullable: true })
  updatedAt: Date;

  @BeforeInsert()
  async beforeInsert() {
    if (!this.createdAt) this.createdAt = new Date();
    if (!this.updatedAt) this.updatedAt = new Date();
  }

  @BeforeUpdate()
  async beforeUpdate() {
    if (!this.updatedAt) this.updatedAt = new Date();
  }
}

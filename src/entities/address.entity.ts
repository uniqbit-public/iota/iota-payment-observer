import { Entity, PrimaryColumn, Column, BeforeInsert, BeforeUpdate, ManyToOne, JoinColumn } from 'typeorm';
import { Account } from './account.entity';

@Entity()
export class Address {
  setAttr(initObj: any) {
    Object.keys(initObj).forEach(key => {
      this[key] = initObj[key];
    });
    return this;
  }

  @PrimaryColumn({ nullable: false })
  address: string;

  @ManyToOne(() => Account, account => account.accountid, { onDelete: 'CASCADE', onUpdate: 'CASCADE' })
  @JoinColumn({ name: 'account' })
  @Column({ nullable: false })
  account: string;

  @Column({ type: 'decimal', nullable: true })
  balance?: number;

  @Column({ nullable: true })
  data?: string;

  @Column({ nullable: true })
  isCancel?: boolean;

  @Column({ type: 'decimal', nullable: true })
  lastReportedBalance?: number;

  @Column({ nullable: true })
  isSuccess?: boolean;

  @Column({ nullable: true })
  resolvedAt?: Date;

  @Column({ nullable: true })
  collectedAt?: Date;

  @Column({ nullable: true })
  reportedAt?: Date;

  @Column({ nullable: true })
  createdAt: Date;

  @Column({ nullable: true })
  updatedAt: Date;

  // tmp
  lookupTime?: number;

  @BeforeInsert()
  async beforeInsert() {
    if (!this.createdAt) this.createdAt = new Date();
    if (!this.updatedAt) this.updatedAt = new Date();
  }

  @BeforeUpdate()
  async beforeUpdate() {
    if (!this.updatedAt) this.updatedAt = new Date();
  }
}

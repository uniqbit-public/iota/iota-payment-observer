import { Entity, PrimaryGeneratedColumn, Column, BeforeInsert, BeforeUpdate, OneToMany } from 'typeorm';
import { Account } from './account.entity';

@Entity()
export class Wallet {
  setAttr(initObj: any) {
    Object.keys(initObj).forEach(key => {
      this[key] = initObj[key];
    });
    return this;
  }

  @PrimaryGeneratedColumn('uuid')
  walletid: string;

  @Column({ nullable: false, select: false })
  mnemonic?: string;

  @Column({ nullable: false, select: false })
  seed?: string;

  @Column({ nullable: true })
  type?: string; // testnet || mainnet

  @Column({ nullable: true })
  createdAt: Date;

  @Column({ nullable: true })
  updatedAt: Date;

  @OneToMany(() => Account, account => account.wallet)
  accounts: Account[];

  @BeforeInsert()
  async beforeInsert() {
    if (!this.createdAt) this.createdAt = new Date();
    if (!this.updatedAt) this.updatedAt = new Date();
  }

  @BeforeUpdate()
  async beforeUpdate() {
    if (!this.updatedAt) this.updatedAt = new Date();
  }
}

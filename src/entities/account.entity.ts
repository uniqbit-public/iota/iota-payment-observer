import { Entity, PrimaryGeneratedColumn, Column, BeforeInsert, BeforeUpdate, OneToMany, ManyToOne, JoinColumn } from 'typeorm';
import { Wallet } from './wallet.entity';
import { Address } from './address.entity';

@Entity()
export class Account {
  setAttr(initObj: any) {
    Object.keys(initObj).forEach(key => {
      this[key] = initObj[key];
    });
    return this;
  }

  @PrimaryGeneratedColumn('uuid')
  accountid: string;

  @ManyToOne(() => Wallet, wallet => wallet.walletid, { onDelete: 'CASCADE', onUpdate: 'CASCADE' })
  @JoinColumn({ name: 'wallet' })
  @Column({ nullable: false })
  wallet: string;

  @Column({ nullable: true })
  createdAt: Date;

  @Column({ nullable: true })
  updatedAt: Date;

  @OneToMany(() => Address, address => address.account)
  addresses: Address[];

  @BeforeInsert()
  async beforeInsert() {
    if (!this.createdAt) this.createdAt = new Date();
    if (!this.updatedAt) this.updatedAt = new Date();
  }

  @BeforeUpdate()
  async beforeUpdate() {
    if (!this.updatedAt) this.updatedAt = new Date();
  }
}

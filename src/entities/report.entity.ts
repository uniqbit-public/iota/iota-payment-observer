import { Entity, PrimaryGeneratedColumn, Column, BeforeInsert, BeforeUpdate } from 'typeorm';

@Entity()
export class Report {
  setAttr(initObj: any) {
    Object.keys(initObj).forEach(key => {
      this[key] = initObj[key];
    });
    return this;
  }

  @PrimaryGeneratedColumn('uuid')
  reportid: string;

  @Column({ nullable: false })
  address: string;

  @Column({ type: 'decimal', nullable: true })
  balance?: number;

  @Column({ nullable: true })
  data?: string;

  @Column({ nullable: true })
  transactionDetails?: string;

  @Column({ nullable: true })
  createdAt: Date;

  @Column({ nullable: true })
  updatedAt: Date;

  @BeforeInsert()
  async beforeInsert() {
    if (!this.createdAt) this.createdAt = new Date();
    if (!this.updatedAt) this.updatedAt = new Date();
  }

  @BeforeUpdate()
  async beforeUpdate() {
    if (!this.updatedAt) this.updatedAt = new Date();
  }
}

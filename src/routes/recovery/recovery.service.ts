import { Injectable } from '@nestjs/common';
import { ErrorService } from '../../providers/services/error.service';
import { LoggingService } from '../../providers/services/logging.service';
import { WalletService } from '../../providers/services/wallet.service';

@Injectable()
export class RecoveryService {
  loggerContext = 'RecoveryService';

  constructor(
    private readonly logger: LoggingService,
    private readonly walletService: WalletService
  ) {}

  async recoverTokens(req: any, body: any): Promise<any> {
    return new Promise(async (resolve, reject) => {
      const collector = body.collectorAddress;
      const recipient = body.recipientAddress;
      const seedEncryptionPw = body.seedEncryptionPw;
      const seed = body.encryptedSeed;
      const network = body.network;

      const balanceObj = await this.walletService
        .getAddressBalance(recipient, network)
        .catch((err) => this.logger.error(err, this.loggerContext));

      this.logger.debug(
        'recovering tokens from address: ' + recipient,
        this.loggerContext
      );
      this.logger.debug('recovering balance:', this.loggerContext);
      this.logger.debug(balanceObj, this.loggerContext);

      if (balanceObj == undefined || parseFloat(balanceObj.balance) <= 0) {
        return reject(new Error('This recipient address is empty'));
      }
      const balance = balanceObj.balance;

      const result = await this.walletService
        .recoverTokensToCollectorAddress(
          balance,
          collector,
          recipient,
          seedEncryptionPw,
          seed,
          network
        )
        .catch((err) => this.logger.error(err, this.loggerContext));

      resolve(result);
    }).catch((err) => ErrorService.getInst().handle(err));
  }
}

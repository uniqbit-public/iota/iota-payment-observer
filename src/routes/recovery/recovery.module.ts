import { Module } from '@nestjs/common';
import { RecoveryController } from './recovery.controller';
import { RecoveryService } from './recovery.service';
import { GlobalProvidersModule } from '../../providers/global-providers.module';

const MODULES = [GlobalProvidersModule];

const CONTROLLERS = [RecoveryController];

const SERVICES = [RecoveryService];

@Module({
  imports: [...MODULES],
  controllers: [...CONTROLLERS],
  providers: [...SERVICES],
  exports: [],
})
export class RecoveryModule {}

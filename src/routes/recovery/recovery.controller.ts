import { Controller, Get, Req, Query, Post, Body } from '@nestjs/common';
import { ApiVersion } from '../../providers/guards';
import { RecoveryService } from './recovery.service';

@Controller('recovery')
export class RecoveryController {
  constructor(private readonly recoveryService: RecoveryService) {}

  @Post()
  @ApiVersion('v1')
  async recoverTokens(@Req() req: any, @Body() body: any): Promise<any> {
    return await this.recoveryService.recoverTokens(req, body);
  }
}

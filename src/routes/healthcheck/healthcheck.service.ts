import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { HealthCheck } from '../../entities/healthcheck.entity';

@Injectable()
export class HealthCheckService {
  constructor(
    @InjectRepository(HealthCheck)
    private readonly healthCheckRepository: Repository<HealthCheck>,
  ) { }

  async getHealthCheck(): Promise<any> {
    const newHealthCheckObj = new HealthCheck();

    await this.healthCheckRepository.save(newHealthCheckObj).catch(err => {
      throw err;
    });
    this.healthCheckRepository.delete({});

    return newHealthCheckObj;
  }
}

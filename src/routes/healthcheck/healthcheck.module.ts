import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { HealthCheckController } from './healthcheck.controller';
import { HealthCheckService } from './healthcheck.service';
import { HealthCheck } from '../../entities/healthcheck.entity';
import { GlobalProvidersModule } from '../../providers/global-providers.module';

const MODULES = [
  GlobalProvidersModule,
  TypeOrmModule.forFeature([
    HealthCheck,
  ]),
];

const CONTROLLERS = [
  HealthCheckController,
];

const SERVICES = [
  HealthCheckService,
];

@Module({
  imports: [...MODULES],
  controllers: [...CONTROLLERS],
  providers: [...SERVICES],
  exports: [],
})
export class HealthCheckModule { }

import { Module } from '@nestjs/common';
import { PriceController } from './price.controller';
import { PriceService } from './price.service';
import { GlobalProvidersModule } from '../../providers/global-providers.module';

const MODULES = [GlobalProvidersModule];

const CONTROLLERS = [PriceController];

const SERVICES = [PriceService];

@Module({
  imports: [...MODULES],
  controllers: [...CONTROLLERS],
  providers: [...SERVICES],
  exports: [],
})
export class PriceModule {}

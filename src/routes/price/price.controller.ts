import { Controller, Get, Query } from '@nestjs/common';
import { ApiVersion } from '../../providers/guards';
import { PriceService } from './price.service';

@Controller('prices')
export class PriceController {
  constructor(
    private readonly priceService: PriceService,
  ) { }

  @Get()
  @ApiVersion('v1')
  async getCurrentIOTAPrice(@Query() query: any): Promise<any> {
    return await this.priceService.getCurrentIOTAPrice(query);
  }
}

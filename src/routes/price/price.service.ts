import { Injectable } from '@nestjs/common';
import { ErrorService } from '../../providers/services/error.service';
import * as cryptoprice from 'crypto-price';
import { LoggingService } from '../../providers/services/logging.service';

@Injectable()
export class PriceService {
  constructor(
    private readonly logger: LoggingService
  ) {}

  async getCurrentIOTAPrice(query: any): Promise<any> {
    return new Promise(async (resolve, reject) => {
      const symbol = query.symbol || 'USD';

      const price = await cryptoprice
        .getCryptoPrice(symbol, 'IOTA')
        .then((obj: any) => {
          // Base for ex - USD, Crypto for ex - ETH
          return parseFloat(obj.price);
        })
        .catch((err: any) => {
          this.logger.error(err);
        });

      return resolve({
        price,
        symbol,
      });
    }).catch((err) => ErrorService.getInst().handle(err));
  }
}

import { Controller, Get } from '@nestjs/common';
import { ApiVersion } from '../../providers/guards';
import { LegalService } from './legal.service';

@Controller('legals')
export class LegalController {
  constructor(
    private readonly legalService: LegalService,
  ) { }

  @Get('/data')
  @ApiVersion('v1')
  async getDataPolicyUrl(): Promise<any> {
    return await this.legalService.getDataPolicyUrl();
  }

  @Get('/notice')
  @ApiVersion('v1')
  async getLegalNoticeUrl(): Promise<any> {
    return await this.legalService.getLegalNoticeUrl();
  }
}

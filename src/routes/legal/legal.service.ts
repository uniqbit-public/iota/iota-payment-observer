import { Injectable } from '@nestjs/common';
import { ErrorService } from '../../providers/services/error.service';
import { GlobalConfigService } from '../../providers/services/config.service';

@Injectable()
export class LegalService {
  constructor(private readonly configService: GlobalConfigService) {}

  async getDataPolicyUrl(): Promise<any> {
    return new Promise(async (resolve, reject) => {
      const dataPolicyUrl = this.configService.getValue(
        'DATA_POLICY_URL',
        false
      );
      if (dataPolicyUrl == undefined || !dataPolicyUrl.length) {
        reject({
          error: new Error('no DATA_POLICY_URL provided'),
          status: 404,
        });
      } else {
        resolve({ url: dataPolicyUrl });
      }
    }).catch((err) => ErrorService.getInst().handle(err));
  }

  async getLegalNoticeUrl(): Promise<any> {
    return new Promise(async (resolve, reject) => {
      const legalNoticeUrl = this.configService.getValue(
        'LEGAL_NOTICE_URL',
        false
      );
      if (legalNoticeUrl == undefined || !legalNoticeUrl.length) {
        reject({
          error: new Error('no LEGAL_NOTICE_URL provided'),
          status: 404,
        });
      } else {
        resolve({ url: legalNoticeUrl });
      }
    }).catch((err) => ErrorService.getInst().handle(err));
  }
}

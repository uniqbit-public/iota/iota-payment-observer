import { Module } from '@nestjs/common';
import { LegalController } from './legal.controller';
import { LegalService } from './legal.service';
import { GlobalProvidersModule } from '../../providers/global-providers.module';

const MODULES = [GlobalProvidersModule];

const CONTROLLERS = [LegalController];

const SERVICES = [LegalService];

@Module({
  imports: [...MODULES],
  controllers: [...CONTROLLERS],
  providers: [...SERVICES],
  exports: [],
})
export class LegalModule {}

import { Controller, Get, Req, Query, Post, Body } from '@nestjs/common';
import { ApiVersion } from '../../providers/guards';
import { TestService } from './test.service';

/**
 * This routes do only exist for testing purposes; The main purpose is to log stuff,
 * do not report back anything internal
 *
 * @export
 * @class TestController
 */
@Controller('test')
export class TestController {
  constructor(
    private readonly testService: TestService,
  ) { }

  @Get('datavalidation')
  @ApiVersion('v1')
  async validateData(@Req() req: any, @Query() query: any): Promise<any> {
    return await this.testService.validateData(req, query);
  }

  @Get('start')
  @ApiVersion('v1')
  async startTransaction(@Req() req: any, @Query() query: any): Promise<any> {
    return await this.testService.startTransaction(req, query);
  }

  @Get('complete')
  @ApiVersion('v1')
  async completeTransaction(@Req() req: any, @Query() query: any): Promise<any> {
    return await this.testService.completeTransaction(req, query);
  }

  @Get('partial')
  @ApiVersion('v1')
  async partialTransaction(@Req() req: any, @Query() query: any): Promise<any> {
    return await this.testService.partialTransaction(req, query);
  }

  @Get('canceled')
  @ApiVersion('v1')
  async canceledTransaction(@Req() req: any, @Query() query: any): Promise<any> {
    return await this.testService.canceledTransaction(req, query);
  }

  @Post('report')
  @ApiVersion('v1')
  async reportTransaction(@Req() req: any, @Query() query: any, @Body() body: any): Promise<any> {
    return await this.testService.reportTransaction(req, query, body);
  }
}

import { Injectable } from '@nestjs/common';
import { ErrorService } from '../../providers/services/error.service';
import { LoggingService } from '../../providers/services/logging.service';
import { dataParse } from '../../libraries/parser.lib';

@Injectable()
export class TestService {
  loggerContext = 'TestService';

  constructor(private readonly logger: LoggingService) {}

  /**
   * test data validation webhook
   *
   * @param {*} req
   * @param {*} query
   * @return {*}  {Promise<any>}
   * @memberof TestService
   */
  async validateData(req: any, query: any): Promise<any> {
    return new Promise(async (resolve, reject) => {
      const secret = query.secret;
      const data = query.data;
      const parsedData = dataParse(data);
      if (parsedData == undefined) {
        this.logger.warn(
          '[test-webhook] validateData could not parse data ' + data,
          this.loggerContext
        );
        throw reject(new Error('no data for validation provided'));
      } else {
        if (!(secret == undefined)) {
          this.logger.debug(
            '[test-webhook] webhook TX_DATA_VALIDATION_WEBHOOK secret ' +
              secret,
            this.loggerContext
          );
        }
        this.logger.debug(
          '[test-webhook] webhook TX_DATA_VALIDATION_WEBHOOK data ' + data,
          this.loggerContext
        );
        this.logger.debug(parsedData, this.loggerContext);
      }
      resolve(data);
    }).catch((err) => ErrorService.getInst().handle(err));
  }

  /**
   * test start tx webhook
   *
   * @param {*} req
   * @param {*} query
   * @return {*}  {Promise<any>}
   * @memberof TestService
   */
  async startTransaction(req: any, query: any): Promise<any> {
    return new Promise(async (resolve, reject) => {
      const secret = query.secret;
      const data = query.data;
      const address = query.address;
      if (!(secret == undefined)) {
        this.logger.debug(
          '[test-webhook] start transaction secret ' + secret,
          this.loggerContext
        );
      }
      if (!(address == undefined)) {
        this.logger.debug(
          '[test-webhook] start transaction address ' + address,
          this.loggerContext
        );
      }
      if (!(data == undefined)) {
        this.logger.debug(
          '[test-webhook] start transaction data ' + data,
          this.loggerContext
        );
      }
      resolve(undefined);
    }).catch((err) => ErrorService.getInst().handle(err));
  }

  /**
   * test complete tx webhook
   *
   * @param {*} req
   * @param {*} query
   * @return {*}  {Promise<any>}
   * @memberof TestService
   */
  async completeTransaction(req: any, query: any): Promise<any> {
    return new Promise(async (resolve, reject) => {
      const secret = query.secret;
      const address = query.address;
      const balance = query.balance;
      const data = query.data;
      const resolvedAt = query.resolvedAt;
      if (!(secret == undefined)) {
        this.logger.debug(
          '[test-webhook] complete transaction secret ' + secret,
          this.loggerContext
        );
      }
      if (!(address == undefined)) {
        this.logger.debug(
          '[test-webhook] complete transaction address ' + address,
          this.loggerContext
        );
      }
      if (!(balance == undefined)) {
        this.logger.debug(
          '[test-webhook] complete transaction balance ' + balance,
          this.loggerContext
        );
      }
      if (!(data == undefined)) {
        this.logger.debug(
          '[test-webhook] complete transaction data ' + data,
          this.loggerContext
        );
      }
      if (!(resolvedAt == undefined)) {
        this.logger.debug(
          '[test-webhook] complete transaction resolvedAt ' + resolvedAt,
          this.loggerContext
        );
      }
      resolve(undefined);
    }).catch((err) => ErrorService.getInst().handle(err));
  }

  /**
   * test partial tx webhook
   *
   * @param {*} req
   * @param {*} query
   * @return {*}  {Promise<any>}
   * @memberof TestService
   */
  async partialTransaction(req: any, query: any): Promise<any> {
    return new Promise(async (resolve, reject) => {
      const secret = query.secret;
      const address = query.address;
      const balance = query.balance;
      const data = query.data;
      if (!(secret == undefined)) {
        this.logger.debug(
          '[test-webhook] partial transaction secret ' + secret,
          this.loggerContext
        );
      }
      if (!(address == undefined)) {
        this.logger.debug(
          '[test-webhook] partial transaction address ' + address,
          this.loggerContext
        );
      }
      if (!(balance == undefined)) {
        this.logger.debug(
          '[test-webhook] partial transaction balance ' + balance,
          this.loggerContext
        );
      }
      if (!(data == undefined)) {
        this.logger.debug(
          '[test-webhook] partial transaction data ' + data,
          this.loggerContext
        );
      }
      resolve(undefined);
    }).catch((err) => ErrorService.getInst().handle(err));
  }

  /**
   * test canceled webhook
   *
   * @param {*} req
   * @param {*} query
   * @return {*}  {Promise<any>}
   * @memberof TestService
   */
  async canceledTransaction(req: any, query: any): Promise<any> {
    return new Promise(async (resolve, reject) => {
      const secret = query.secret;
      const address = query.address;
      const balance = query.balance;
      const data = query.data;
      if (!(secret == undefined)) {
        this.logger.debug(
          '[test-webhook] canceled transaction secret ' + secret,
          this.loggerContext
        );
      }
      if (!(address == undefined)) {
        this.logger.debug(
          '[test-webhook] canceled transaction address ' + address,
          this.loggerContext
        );
      }
      if (!(balance == undefined)) {
        this.logger.debug(
          '[test-webhook] canceled transaction balance ' + balance,
          this.loggerContext
        );
      }
      if (!(data == undefined)) {
        this.logger.debug(
          '[test-webhook] canceled transaction data ' + data,
          this.loggerContext
        );
      }
      resolve(undefined);
    }).catch((err) => ErrorService.getInst().handle(err));
  }

  /**
   * Test reporting webhook
   *
   * @param {*} req
   * @param {*} query
   * @param {*} body
   * @return {*}  {Promise<any>}
   * @memberof TestService
   */
  async reportTransaction(req: any, query: any, body: any): Promise<any> {
    return new Promise(async (resolve, reject) => {
      const secret = query.secret;
      const address = query.address;
      if (!(secret == undefined)) {
        this.logger.debug(
          '[test-webhook] report transaction secret ' + secret,
          this.loggerContext
        );
      }
      if (!(address == undefined)) {
        this.logger.debug(
          '[test-webhook] report transaction address ' + address,
          this.loggerContext
        );
      }
      if (!(body == undefined)) {
        this.logger.debug(
          '[test-webhook] report transaction payload:',
          this.loggerContext
        );
        this.logger.debug(body, this.loggerContext);
      }
      resolve(undefined);
    }).catch((err) => ErrorService.getInst().handle(err));
  }
}

import { Controller, Get, Req, Query, Param, Patch, Body } from '@nestjs/common';
import { ApiVersion } from '../../providers/guards';
import { AddressService } from './address.service';

@Controller('addresses')
export class AddressController {
  constructor(
    private readonly addressService: AddressService,
  ) { }

  @Get()
  @ApiVersion('v1')
  async getNextAddress(@Req() req: any, @Query() query: any): Promise<any> {
    return await this.addressService.getNextAddress(req, query);
  }

  @Get('/:address')
  @ApiVersion('v1')
  async getAddressStatus(@Req() req: any, @Param() param: any, @Query() query: any): Promise<any> {
    return await this.addressService.getAddressStatus(req, param, query);
  }

  @Patch('/cancel/:address')
  @ApiVersion('v1')
  async cancelAddress(@Req() req: any, @Param() param: any, @Body() body: any): Promise<any> {
    return await this.addressService.cancelAddress(req, param, body);
  }

  @Patch('/collect/:address')
  @ApiVersion('v1')
  async collectAddress(@Req() req: any, @Param() param: any, @Body() body: any): Promise<any> {
    return await this.addressService.collectAddress(req, param, body);
  }
}

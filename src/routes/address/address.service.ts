import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Address } from '../../entities/address.entity';
import { ErrorService } from '../../providers/services/error.service';
import { WalletService } from '../../providers/services/wallet.service';
import { WebhookService } from '../../providers/services/webhook.service';
import { LoggingService } from '../../providers/services/logging.service';
import { getExpectedValue } from '../../libraries/parser.lib';

@Injectable()
export class AddressService {
  loggerContext = 'AddressService';
  private readonly maxAddressLastUpdateTimeDiff = 1250;

  constructor(
    @InjectRepository(Address)
    private readonly addressRepository: Repository<Address>,
    private readonly walletService: WalletService,
    private readonly logger: LoggingService,
    private readonly webhookService: WebhookService
  ) {}

  async getNextAddress(req: any, query: any): Promise<any> {
    return new Promise(async (resolve, reject) => {
      let data = query.data;
      let errorState: any;

      data = await this.webhookService
        .sendDataValidationWebhook(data)
        .catch((err) => {
          errorState = err;
        });
      if (!(errorState == undefined)) {
        return reject(errorState);
      }

      const addressObj = (await this.walletService
        .getNextAddress(data)
        .catch((err) => {
          reject(err);
        })) as Address;
      if (!(addressObj == undefined)) {
        addressObj.data = data;
      }

      this.webhookService.sendStartWebhook(addressObj);
      resolve(addressObj);
    }).catch((err) => ErrorService.getInst().handle(err));
  }

  async getAddressStatus(req: any, param: any, query: any): Promise<any> {
    return new Promise(async (resolve, reject) => {
      if (param == undefined) {
        const errMsg = 'param is undefined';
        this.logger.error(errMsg);
        return reject(new Error(errMsg));
      }
      const startTime = new Date().valueOf();
      const address = param.address;
      const qb = this.addressRepository.createQueryBuilder('address');
      qb.where('address.address = :address', { address });
      const addressObj = (await qb.getOne().catch((err) => {
        this.logger.error(err);
        reject(err);
      })) as Address;
      if (addressObj == undefined) {
        return;
      }

      // TESTING
      // console.info('getAddressStatus', addressObj);

      // if collectedAt was set, then the collector did already verify the balance
      if (
        new Date().valueOf() - new Date(addressObj.updatedAt).valueOf() >
          this.maxAddressLastUpdateTimeDiff &&
        addressObj.collectedAt == undefined
      ) {
        const balanceObj = await this.walletService
          .getAddressBalance(addressObj.address)
          .catch((err) => {
            this.logger.error(err, this.loggerContext);
          });

        // TESTING
        // console.info('balanceObj', balanceObj);

        if (balanceObj == undefined) {
          this.logger.error(
            'Could not get recipient address balance',
            this.loggerContext
          );
          return;
        }
        const newBalance = parseFloat(balanceObj.balance as any);
        const oldBalance = parseFloat(addressObj.balance as any);

        if (oldBalance !== newBalance) {
          addressObj.balance = newBalance;
          addressObj.updatedAt = new Date();

          // check if transaction is partially filled or completed
          if (
            newBalance >= getExpectedValue(addressObj.data) &&
            newBalance > 0
          ) {
            addressObj.isSuccess = true;
            if (addressObj.resolvedAt == undefined) {
              addressObj.resolvedAt = new Date();
              this.webhookService.sendCompleteWebhook(addressObj);
            }
          } else if (
            parseFloat((addressObj.lastReportedBalance || 0) as any) !==
            parseFloat(addressObj.balance as any)
          ) {
            addressObj.lastReportedBalance = parseFloat(
              addressObj.balance as any
            );
            this.webhookService.sendPartialWebhook(addressObj);
          }

          const resultAddressObj = await this.addressRepository
            .update(addressObj.address, addressObj)
            .catch((err) => {
              reject(err);
            });
          if (resultAddressObj == undefined) {
            return;
          }
        }
      }

      addressObj.lookupTime = new Date().valueOf() - startTime;
      resolve(addressObj);
    }).catch((err) => ErrorService.getInst().handle(err));
  }

  async cancelAddress(req: any, param: any, body: any): Promise<any> {
    return new Promise(async (resolve, reject) => {
      const address = param.address;
      const isCancel = body.isCancel;

      const addressObj = (await this.addressRepository
        .findOne(address)
        .catch((err) => {
          reject(err);
        })) as Address;
      if (addressObj == undefined) {
        return;
      }

      if (addressObj.resolvedAt == undefined) {
        addressObj.resolvedAt = new Date();
        this.webhookService.sendCanceledWebhook(addressObj);
      }

      addressObj.isCancel = isCancel;
      addressObj.updatedAt = new Date();
      await this.addressRepository
        .update(addressObj.address, addressObj)
        .catch((err) => {
          reject(err);
        });

      resolve('canceled address ' + address);
    }).catch((err) => ErrorService.getInst().handle(err));
  }

  async collectAddress(req: any, param: any, body: any): Promise<any> {
    return new Promise(async (resolve, reject) => {
      const address = param.address;

      const addressObj = (await this.addressRepository
        .findOne(address)
        .catch((err) => {
          reject(err);
        })) as Address;
      if (addressObj == undefined) {
        return resolve('address does not exist');
      }

      const colletResult = await this.walletService
        .sendToCollectorAddress(addressObj)
        .catch((err) => {
          reject(err);
        });
      if (colletResult == undefined) {
        return resolve('could not collect balance');
      }

      addressObj.collectedAt = new Date();
      addressObj.updatedAt = new Date();
      const addressUpdateResult = await this.addressRepository
        .update(addressObj.address, addressObj)
        .catch((err) => {
          reject(err);
        });
      if (addressUpdateResult == undefined) {
        return resolve('address update failed');
      }

      resolve({
        message: 'collected all token belonging to seed of address ' + address,
        result: colletResult,
      });
    }).catch((err) => ErrorService.getInst().handle(err));
  }
}

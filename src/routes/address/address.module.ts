import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AddressController } from './address.controller';
import { AddressService } from './address.service';
import { Wallet } from '../../entities/wallet.entity';
import { Account } from '../../entities/account.entity';
import { Address } from '../../entities/address.entity';
import { GlobalProvidersModule } from '../../providers/global-providers.module';

const MODULES = [
  GlobalProvidersModule,
  TypeOrmModule.forFeature([
    Wallet,
    Account,
    Address
  ]),
];

const CONTROLLERS = [
  AddressController,
];

const SERVICES = [
  AddressService,
];

@Module({
  imports: [...MODULES],
  controllers: [...CONTROLLERS],
  providers: [...SERVICES],
  exports: [],
})
export class AddressModule { }

import * as crypto from 'crypto';
import { getEnvVar } from './dot-env.lib';

let PLAIN_PW =
  process.env.SEED_ENCRYPTION_PW || getEnvVar('SEED_ENCRYPTION_PW');
let ENCRYPTION_KEY: string;
const IV_LENGTH = 16; // For AES, this is always 16
const algorithm = 'aes-256-ctr'; // DEPRECATED "aes-128-cbc"

const ensurePw = () => {
  if (ENCRYPTION_KEY == undefined) {
    if (PLAIN_PW == undefined) {
      throw new Error('no crypto password provided');
    }
    ENCRYPTION_KEY = crypto
      .createHash('sha256')
      .update(String(PLAIN_PW))
      .digest('base64')
      .substr(0, 32);
  }
};

export const encrypt = (text: string) => {
  ensurePw();
  const iv = crypto
    .randomBytes(IV_LENGTH)
    .toString('hex')
    .slice(0, 16);
  const cipher = crypto.createCipheriv(algorithm, ENCRYPTION_KEY, iv);
  let encrypted = cipher.update(text);
  encrypted = Buffer.concat([encrypted, cipher.final()]);
  return iv + ':' + encrypted.toString('hex');
};

export const decrypt = (text: string, customPw?: string) => {
  ensurePw();
  const textParts = text.split(':');
  const iv = textParts[0];
  const encryptedText = Buffer.from(textParts[1], 'hex');
  let decipher: crypto.Decipher;
  if (!(customPw == undefined) && customPw.length) {
    const customEncryptionKey = crypto
      .createHash('sha256')
      .update(String(customPw))
      .digest('base64')
      .substr(0, 32);
    decipher = crypto.createDecipheriv(algorithm, customEncryptionKey, iv);
  } else {
    decipher = crypto.createDecipheriv(algorithm, ENCRYPTION_KEY, iv);
  }
  let decrypted = decipher.update(encryptedText);
  decrypted = Buffer.concat([decrypted, decipher.final()]);
  return decrypted.toString();
};

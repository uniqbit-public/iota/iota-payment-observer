import { btoa } from './base64.lib';

export const getExpectedValue = (data: string) => {
  let expectedValue = 0;
  if (!(data == undefined) && data.length) {
    try {
      const decodedData = dataParse(data);
      if (!(decodedData == undefined) && !Number.isNaN(decodedData.value)) {
        expectedValue = parseFloat(decodedData.value || 0);
        expectedValue = expectedValue * 0.999; // expect 0.1% less so that exact amounts will pass
      }
    } catch (e) {
      console.error(e);
    }
  }
  return expectedValue;
};

export const dataParse = (data: string) => {
  if (!(data == undefined) && data.length) {
    try {
      return JSON.parse(btoa(data));
    } catch (e) {
      console.error(e);
    }
  }
};

import { Injectable } from '@nestjs/common';
import { Cron, CronExpression, SchedulerRegistry } from '@nestjs/schedule';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { GlobalConfigService } from '../services/config.service';
import { Wallet } from '../../entities/wallet.entity';
import { Address } from '../../entities/address.entity';
import { Archive } from '../../entities/archive.entity';
import { WalletService } from '../services/wallet.service';
import { LoggingService } from '../services/logging.service';

@Injectable()
export class CleanerAgent {
  isAgentRunning = false;

  archiveLastUpdateTimeDifference = 1000 * 60 * 60 * 24 * 30;

  loggerContext = 'CleanerAgent';

  constructor(
    @InjectRepository(Wallet)
    private readonly walletRepository: Repository<Wallet>,
    @InjectRepository(Address)
    private readonly addressRepository: Repository<Address>,
    @InjectRepository(Archive)
    private readonly archiveRepository: Repository<Archive>,
    private readonly schedulerRegistry: SchedulerRegistry,
    private readonly logger: LoggingService,
    private readonly walletService: WalletService
  ) {
    // init upon api start, don't wait for first run
    setTimeout(async () => {
      this.runAgent();
    }, 500);
  }

  /**
   * The job of this agent is to archive old seeds and remove them from active wallets.
   * Double check seed balance and transfer it before archiving.
   *
   * @return {*}  {Promise<void>}
   * @memberof CleanerAgent
   */
  @Cron(
    (GlobalConfigService.currentEnv === 'DEV'
      ? CronExpression.EVERY_5_MINUTES
      : CronExpression.EVERY_6_HOURS) + '',
    {
      name: 'cleaner_agent_cron_job',
    }
  )
  async runAgent(): Promise<void> {
    if (!this.isAgentRunning) {
      this.isAgentRunning = true;
      this.logger.debug(
        'cleaner agent, starting cronjob...',
        this.loggerContext
      );

      const archiveCheckDate = new Date(
        new Date().valueOf() - this.archiveLastUpdateTimeDifference
      );

      const qb = this.addressRepository.createQueryBuilder('address');
      qb.leftJoinAndSelect('address.account', 'account');
      qb.leftJoinAndSelect('account.wallet', 'wallet');
      qb.select([
        'address.updatedAt',
        'address.address',
        'account.accountid',
        'wallet.walletid',
        'wallet.seed',
        'wallet.type',
        'wallet.mnemonic',
      ]);
      qb.where('address.updatedAt < :archiveCheckDate', { archiveCheckDate });

      const addressObjs = (await qb
        .getMany()
        .catch((err) => this.logger.error(err))) as Address[];
      if (addressObjs == undefined) {
        return;
      }

      for (const addressObj of addressObjs) {
        this.logger.debug(
          'Archiving address ' + addressObj.address,
          this.loggerContext
        );
        await this.walletService
          .sendToCollectorAddress(addressObj)
          .catch((err) => this.logger.warn(err.message, this.loggerContext));
        const newArchiveObj = new Archive().setAttr({
          mnemonic: addressObj.account['wallet']['mnemonic'],
          seed: addressObj.account['wallet']['seed'],
          type: addressObj.account['wallet']['type'],
        });
        await this.archiveRepository
          .save(newArchiveObj)
          .catch((err) => this.logger.error(err));
        await this.walletRepository
          .delete({
            walletid: addressObj.account['wallet']['walletid'],
          })
          .catch((err) => this.logger.error(err));
      }

      this.isAgentRunning = false;
    }
  }

  stopAgent(): void {
    const job = this.schedulerRegistry.getCronJob('cleaner_agent_cron_job');
    job.stop();
    this.logger.debug('Stopped cleaner agent cron job.', this.loggerContext);
  }

  startAgent(): void {
    const job = this.schedulerRegistry.getCronJob('cleaner_agent_cron_job');
    job.start();
    this.logger.debug(
      'cleaner agent cron job was started.',
      this.loggerContext
    );
  }

  restartAgent(): void {
    this.stopAgent();
    this.startAgent();
  }
}

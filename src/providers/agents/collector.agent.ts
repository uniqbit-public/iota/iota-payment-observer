import { Injectable } from '@nestjs/common';
import { Cron, CronExpression, SchedulerRegistry } from '@nestjs/schedule';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { GlobalConfigService } from '../services/config.service';
import { LoggingService } from '../services/logging.service';
import { Address } from '../../entities/address.entity';
import { WalletService } from '../services/wallet.service';
import { WebhookService } from '../services/webhook.service';
import { getExpectedValue } from '../../libraries/parser.lib';

@Injectable()
export class CollectorAgent {
  isAgentRunning = false;

  minSuccessDelay = 1000 * 60 * 60 * 2;
  minCancelDelay = 1000 * 60 * 30;
  maxCancelFallback = 1000 * 60 * 60 * 24;

  loggerContext = 'CollectorAgent';

  constructor(
    @InjectRepository(Address)
    private readonly addressRepository: Repository<Address>,
    private readonly schedulerRegistry: SchedulerRegistry,
    private readonly logger: LoggingService,
    private readonly walletService: WalletService,
    private readonly webhookService: WebhookService,
    private readonly configService: GlobalConfigService
  ) {
    // init upon api start, don't wait for first run
    setTimeout(async () => {
      this.runAgent();
    }, 500);
  }

  /**
   * The job of this agent is to collect received token and transfer them to the receiver address
   *
   * @return {*}  {Promise<void>}
   * @memberof CollectorAgent
   */
  @Cron(
    (GlobalConfigService.currentEnv === 'DEV'
      ? CronExpression.EVERY_10_SECONDS
      : CronExpression.EVERY_30_SECONDS) + '',
    {
      name: 'collector_agent_cron_job',
    }
  )
  async runAgent(): Promise<void> {
    if (!this.isAgentRunning) {
      this.isAgentRunning = true;
      const network = this.configService.getValue('IOTA_NETWORK');
      this.logger.debug(
        `collector agent on ${network}, starting cronjob...`,
        this.loggerContext
      );

      const qb = this.addressRepository.createQueryBuilder('address');
      qb.leftJoinAndSelect('address.account', 'account');
      qb.leftJoinAndSelect('account.wallet', 'wallet');
      qb.where('address.collectedAt IS NULL');
      qb.andWhere('wallet.type = :network', { network });
      const dueAddressObjs = (await qb
        .getMany()
        .catch((err) =>
          this.logger.error(err, this.loggerContext)
        )) as Address[];
      if (!(dueAddressObjs == undefined)) {
        for (const dueAddressObj of dueAddressObjs) {
          this.logger.debug(
            'is handling address ' + dueAddressObj.address,
            this.loggerContext
          );

          let result = await this.handleCanceledAddressWithBalance(
            dueAddressObj
          );
          if (result) continue;

          result = await this.handleCanceled(dueAddressObj);
          if (result) continue;

          result = await this.handleSuccess(dueAddressObj);
          if (result) continue;

          result = await this.handleNotYetRecognizedSuccess(dueAddressObj);
          if (result) continue;

          result = await this.handlePartialTx(dueAddressObj);
          if (result) continue;

          result = await this.handleObsoleteTx(dueAddressObj);
          if (result) continue;

          this.logger.debug(
            'skipped, waiting for address ' +
              dueAddressObj.address +
              ' to resolve',
            this.loggerContext
          );
        }
      }
      this.isAgentRunning = false;
    }
  }

  stopAgent(): void {
    const job = this.schedulerRegistry.getCronJob('collector_agent_cron_job');
    job.stop();
    this.logger.debug('Stopped collector agent cron job.', this.loggerContext);
  }

  startAgent(): void {
    const job = this.schedulerRegistry.getCronJob('collector_agent_cron_job');
    job.start();
    this.logger.debug(
      'collector agent cron job was started.',
      this.loggerContext
    );
  }

  restartAgent(): void {
    this.stopAgent();
    this.startAgent();
  }

  /**
   * canceled but has balance which needs to be transfered to collector address
   *
   * @param {Address} addressObj
   * @return {*}  {Promise<boolean>}
   * @memberof CollectorAgent
   */
  async handleCanceledAddressWithBalance(
    addressObj: Address
  ): Promise<boolean> {
    // CASE: is canceled but has balance
    if (
      addressObj.collectedAt == undefined &&
      addressObj.isCancel &&
      parseFloat(addressObj.balance as any) > 0 &&
      new Date().valueOf() - new Date(addressObj.updatedAt).valueOf() >
        this.minCancelDelay
    ) {
      this.logger.log(
        'is canceled but has balance ' + addressObj.address,
        this.loggerContext
      );
      const result = await this.walletService
        .sendToCollectorAddress(addressObj)
        .catch((err) => this.logger.error(err, this.loggerContext));
      if (result == undefined || result.balance == undefined) {
        this.logger.warn(
          'is canceled but has balance => result is undefined',
          this.loggerContext
        );
        return false;
      }

      if (addressObj.resolvedAt == undefined) {
        addressObj.resolvedAt = new Date();
        this.webhookService.sendCanceledWebhook(addressObj);
      }
      addressObj.collectedAt = new Date();
      await this.webhookService.sendReportWebhook(addressObj);
      await this.updateAddressEntity(addressObj);
      return true;
    }
    return false;
  }

  /**
   * canceled but without balance
   *
   * @param {Address} addressObj
   * @return {*}  {Promise<boolean>}
   * @memberof CollectorAgent
   */
  async handleCanceled(addressObj: Address): Promise<boolean> {
    if (
      addressObj.collectedAt == undefined &&
      addressObj.isCancel &&
      (addressObj.balance == undefined ||
        parseFloat(addressObj.balance as any) === 0)
    ) {
      this.logger.log('is canceled ' + addressObj.address, this.loggerContext);
      if (addressObj.resolvedAt == undefined) {
        addressObj.resolvedAt = new Date();
        this.webhookService.sendCanceledWebhook(addressObj);
      }
      addressObj.collectedAt = new Date();
      await this.updateAddressEntity(addressObj);
      return true;
    }
    return false;
  }

  /**
   * transfere the balance from successful transactions to collector address and report it
   *
   * @param {Address} addressObj
   * @return {*}  {Promise<boolean>}
   * @memberof CollectorAgent
   */
  async handleSuccess(addressObj: Address): Promise<boolean> {
    if (
      addressObj.collectedAt == undefined &&
      addressObj.isSuccess &&
      parseFloat(addressObj.balance as any) > 0 &&
      new Date().valueOf() - new Date(addressObj.updatedAt).valueOf() >
        this.minSuccessDelay
    ) {
      this.logger.log('is success ' + addressObj.address, this.loggerContext);
      if (!(await this.hasSufficientAddressBalance(addressObj))) {
        return false;
      }

      const result = await this.walletService
        .sendToCollectorAddress(addressObj)
        .catch((err) => this.logger.error(err, this.loggerContext));
      if (result == undefined || result.balance == undefined) {
        this.logger.warn(
          'is success => collection has failed',
          this.loggerContext
        );
        return false;
      }

      if (
        parseFloat(addressObj.balance as any) >=
          getExpectedValue(addressObj.data) &&
        addressObj.resolvedAt == undefined
      ) {
        addressObj.resolvedAt = new Date();
        this.webhookService.sendCompleteWebhook(addressObj);
      }

      addressObj.collectedAt = new Date();
      await this.webhookService.sendReportWebhook(addressObj);
      await this.updateAddressEntity(addressObj);
      return true;
    }
    return false;
  }

  /**
   * is success but not yet recognized
   *
   * @param {Address} addressObj
   * @return {*}  {Promise<boolean>}
   * @memberof CollectorAgent
   */
  async handleNotYetRecognizedSuccess(addressObj: Address): Promise<boolean> {
    if (
      addressObj.collectedAt == undefined &&
      parseFloat(addressObj.balance as any) >=
        getExpectedValue(addressObj.data) &&
      parseFloat(addressObj.balance as any) > 0
    ) {
      this.logger.log(
        'is success but not yet recognized ' + addressObj.address,
        this.loggerContext
      );
      if (!(await this.hasSufficientAddressBalance(addressObj))) {
        return false;
      }

      const result = await this.walletService
        .sendToCollectorAddress(addressObj)
        .catch((err) => this.logger.error(err, this.loggerContext));
      if (result == undefined || result.balance == undefined) {
        this.logger.warn(
          'not yet recognized success => collection has failed',
          this.loggerContext
        );
        return false;
      }

      addressObj.isSuccess = true;
      if (addressObj.resolvedAt == undefined) {
        addressObj.resolvedAt = new Date();
        this.webhookService.sendCompleteWebhook(addressObj);
      }

      addressObj.collectedAt = new Date();
      await this.webhookService.sendReportWebhook(addressObj);
      await this.updateAddressEntity(addressObj);
      return true;
    }
    return false;
  }

  /**
   * is partial transaction; not yet complete
   *
   * @param {Address} addressObj
   * @return {*}  {Promise<boolean>}
   * @memberof CollectorAgent
   */
  async handlePartialTx(addressObj: Address): Promise<boolean> {
    if (
      addressObj.collectedAt == undefined &&
      parseFloat(addressObj.balance as any) > 0 &&
      new Date().valueOf() - new Date(addressObj.updatedAt).valueOf() <
        this.maxCancelFallback
    ) {
      // TESTING
      this.logger.log(
        'is partial transaction but not yet complete ' + addressObj.address,
        this.loggerContext
      );
      if (
        addressObj.lastReportedBalance == undefined ||
        parseFloat(addressObj.lastReportedBalance as any) !==
          parseFloat(addressObj.balance as any)
      ) {
        addressObj.lastReportedBalance = parseFloat(addressObj.balance as any);
        this.webhookService.sendPartialWebhook(addressObj);
        await this.updateAddressEntity(addressObj);
      }
      return true;
    }
    return false;
  }

  /**
   * garbage collect old transactions
   *
   * @param {Address} addressObj
   * @return {*}  {Promise<boolean>}
   * @memberof CollectorAgent
   */
  async handleObsoleteTx(addressObj: Address): Promise<boolean> {
    if (
      addressObj.collectedAt == undefined &&
      new Date().valueOf() - new Date(addressObj.updatedAt).valueOf() >=
        this.maxCancelFallback
    ) {
      this.logger.log(
        'is obsolete transaction ' + addressObj.address,
        this.loggerContext
      );
      if (parseFloat(addressObj.balance as any) > 0) {
        await this.walletService
          .sendToCollectorAddress(addressObj)
          .catch((err) =>
            this.logger.warn('OBSOLETE: ' + err, this.loggerContext)
          );
      }
      addressObj.isCancel = true;
      if (addressObj.resolvedAt == undefined) {
        addressObj.resolvedAt = new Date();
        this.webhookService.sendCanceledWebhook(addressObj);
      }
      addressObj.updatedAt = new Date();
      addressObj.collectedAt = new Date();
      await this.updateAddressEntity(addressObj);
      return true;
    }
    return false;
  }

  /**
   * helper to update addressObjs
   *
   * @param {Address} addressObj
   * @return {*}  {Promise<any>}
   * @memberof CollectorAgent
   */
  async updateAddressEntity(addressObj: Address): Promise<any> {
    addressObj.updatedAt = new Date();
    return await this.addressRepository
      .update(addressObj.address, addressObj)
      .catch((err) => this.logger.error(err, this.loggerContext));
  }

  /**
   * helper to double check actual balance in tangle
   *
   * @param {Address} addressObj
   * @return {*}  {Promise<boolean>}
   * @memberof CollectorAgent
   */
  async hasSufficientAddressBalance(addressObj: Address): Promise<boolean> {
    const addressBalance = await this.walletService
      .getAddressBalance(addressObj.address)
      .catch((err) => this.logger.error(err, this.loggerContext));
    if (addressBalance == undefined) {
      return false;
    }
    const expectedValue = getExpectedValue(addressObj.data);
    if (parseFloat(addressBalance.balance) < expectedValue) {
      this.logger.error(
        'insufficient balance, available/expected: ' +
          addressBalance.balance +
          '/' +
          expectedValue,
        this.loggerContext
      );
      return false;
    }
    return true;
  }
}

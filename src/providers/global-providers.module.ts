import { Module, HttpModule } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ScheduleModule } from '@nestjs/schedule';
import { Wallet } from '../entities/wallet.entity';
import { Account } from '../entities/account.entity';
import { Address } from '../entities/address.entity';
import { Archive } from '../entities/archive.entity';
import { Report } from '../entities/report.entity';
import { APP_GUARD } from '@nestjs/core';
import { ApiVersionGuard } from './guards';
import { Logger } from '@nestjs/common';
import { ErrorService } from './services/error.service';
import { WalletService } from './services/wallet.service';
import { GlobalConfigService } from './services/config.service';
import { WebhookService } from './services/webhook.service';
import { LoggingService } from './services/logging.service';
import { CollectorAgent } from './agents/collector.agent';
import { CleanerAgent } from './agents/cleaner.agent';

const SERVICES = [
  Logger,
  ErrorService,
  WalletService,
  GlobalConfigService,
  WebhookService,
  LoggingService,
  CollectorAgent,
  CleanerAgent,
];

const GUARDS = [
  {
    provide: APP_GUARD,
    useClass: ApiVersionGuard,
  },
];

const MODULES = [
  HttpModule,
  TypeOrmModule.forFeature([Wallet, Account, Address, Archive, Report]),
  ScheduleModule.forRoot(),
];

@Module({
  imports: [...MODULES],
  providers: [...SERVICES, ...GUARDS],
  exports: [...SERVICES],
})
export class GlobalProvidersModule {}

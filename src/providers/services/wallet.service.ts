import { Injectable } from '@nestjs/common';
import { ClientBuilder, Client } from '@iota/client';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { GlobalConfigService } from './config.service';
import { LoggingService } from './logging.service';
import { Wallet } from '../../entities/wallet.entity';
import { Account } from '../../entities/account.entity';
import { Address } from '../../entities/address.entity';
import { decrypt, encrypt } from '../../libraries/crypto.lib';

@Injectable()
export class WalletService {
  private iotaClient: Client;
  private altIotaClient: Client;

  loggerContext = 'WalletService';

  constructor(
    @InjectRepository(Wallet)
    private readonly walletRepository: Repository<Wallet>,
    @InjectRepository(Account)
    private readonly accountRepository: Repository<Account>,
    @InjectRepository(Address)
    private readonly addressRepository: Repository<Address>,
    private readonly logger: LoggingService,
    private readonly configService: GlobalConfigService
  ) {}

  /**
   *
   *
   * @param {string} address
   * @param {string} [forceNetwork]
   * @return {*}  {Promise<any>}
   * @memberof WalletService
   */
  async getAddressBalance(
    address: string,
    forceNetwork?: string
  ): Promise<any> {
    return new Promise(async (resolve, reject) => {
      const client = this.getWalletClient(forceNetwork);

      // TESTING
      // console.info(await client.getInfo());
      // console.info('getAddressBalance of address', address);

      const balance = (await client
        .getAddressBalance(address)
        .catch((err) => this.logger.error(err))) as any;

      if (balance == undefined) {
        const errMsg = 'balance not found';
        this.logger.error(errMsg, this.loggerContext);
        return reject(new Error(errMsg));
      }

      resolve({
        address,
        balance: balance.balance || 0,
      });
    });
  }

  /**
   *
   *
   * @return {*}  {Promise<Address>}
   * @memberof WalletService
   */
  async getNextAddress(data: string): Promise<Address> {
    return new Promise(async (resolve, reject) => {
      let walletObj = (await this.createNewWalletSeed().catch((err) =>
        this.logger.error(err, this.loggerContext)
      )) as Wallet;
      if (walletObj == undefined) {
        return reject({
          error: new Error('could not get a valid seed'),
          code: 404,
        });
      }

      let accountObj = (await this.createNextAccount(walletObj.walletid).catch(
        (err) => {
          reject(err);
        }
      )) as Account;
      if (accountObj == undefined) {
        return reject({
          error: new Error('could not create next account'),
          code: 404,
        });
      }

      const client = this.getWalletClient();
      const addresses = (await client
        .getAddresses(walletObj.seed)
        .accountIndex(0)
        .range(0, 1)
        .get()) as string[];

      const newAddressObj = new Address().setAttr({
        address: addresses[0],
        account: accountObj.accountid,
        data,
      });
      const resultAddressObj = (await this.addressRepository
        .save(newAddressObj)
        .catch((err) => {
          reject(err);
        })) as Address;
      if (resultAddressObj == undefined) {
        return reject(new Error('address retrieval failed'));
      }
      resolve(resultAddressObj);
    });
  }

  /**
   *
   *
   * @param {string} walletid
   * @return {*}  {Promise<Account>}
   * @memberof WalletService
   */
  createNextAccount(walletid: string): Promise<Account> {
    return new Promise(async (resolve, reject) => {
      const newAccountObj = new Account().setAttr({
        wallet: walletid,
      });
      const accountObj = (await this.accountRepository
        .save(newAccountObj)
        .catch((err) => {
          reject(err);
        })) as Account;
      if (accountObj == undefined) {
        return reject(new Error('account retrieval failed'));
      }
      resolve(accountObj);
    });
  }

  /**
   *
   *
   * @return {*}  {Promise<string[]>}
   * @memberof WalletService
   */
  async getAllWalletIDs(): Promise<string[]> {
    return new Promise(async (resolve, reject) => {
      const qb = this.walletRepository.createQueryBuilder('wallet');
      qb.select('wallet.walletid');
      const walletObjs = (await qb.getMany().catch((err) => {
        reject(err);
      })) as Wallet[];
      const walletids = [];
      if (!(walletObjs == undefined) && walletObjs.length) {
        for (const walletObj of walletObjs) {
          walletids.push(walletObj.walletid);
        }
      }
      resolve(walletids);
    });
  }

  /**
   *
   *
   * @private a seed should never leak outside!
   * @param {string} walletid
   * @return {*}  {Promise<any>}
   * @memberof WalletService
   */
  private async getExactWalletSeed(walletid: string): Promise<any> {
    return new Promise(async (resolve, reject) => {
      if (!(walletid == undefined) && walletid.length) {
        const qb = this.walletRepository.createQueryBuilder('wallet');
        qb.select(['wallet.walletid', 'wallet.seed', 'wallet.type']);
        qb.where('wallet.walletid = :walletid', { walletid });
        qb.andWhere('wallet.type = :type', {
          type: this.configService.getValue('IOTA_NETWORK'),
        });
        const walletObj = (await qb.getOne().catch((err) => {
          reject(err);
        })) as Wallet;
        if (!(walletObj == undefined)) {
          return resolve({
            walletid,
            seed: decrypt(walletObj.seed),
          });
        }
      }

      reject(new Error('seed retrieval failed'));
    });
  }

  /**
   *
   *
   * @private a seed should never leak outside!
   * @return {*}  {Promise<any>}
   * @memberof WalletService
   */
  private async createNewWalletSeed(): Promise<any> {
    return new Promise(async (resolve, reject) => {
      const client = this.getWalletClient();
      const mnemonic = client.generateMnemonic();
      const seed = client.mnemonicToHexSeed(mnemonic);
      const newWalletObj = new Wallet().setAttr({
        mnemonic: encrypt(mnemonic),
        seed: encrypt(seed),
        type: this.configService.getValue('IOTA_NETWORK'),
      });

      const resultWalletObj = (await this.walletRepository
        .save(newWalletObj)
        .catch((err) => {
          reject(err);
        })) as Wallet;
      if (!(resultWalletObj == undefined)) {
        return resolve({
          walletid: resultWalletObj.walletid,
          seed,
        });
      }

      reject(new Error('seed retrieval failed'));
    });
  }

  /**
   * ensures that a valid iota client instance is created
   *
   * @return {*}  {Client}
   * @memberof WalletService
   */
  getWalletClient(forceNetwork?: string): Client {
    let network = this.configService.getValue('IOTA_NETWORK');
    const collectorAddress = this.configService.getValue('COLLECTOR_ADDRESS');

    // catch network to address missmatch for env var settings
    const addressPrefix = collectorAddress.split('1')[0];
    if (addressPrefix === 'atoi' && network !== 'testnet') {
      const errMsg =
        'collector address is a testnet one but given target network is ' +
        network;
      this.logger.error(errMsg, this.loggerContext);
      throw new Error(errMsg);
    } else if (addressPrefix === 'iota' && network !== 'mainnet') {
      const errMsg =
        'collector address is a mainnet one but given target network is ' +
        network;
      this.logger.error(errMsg, this.loggerContext);
      throw new Error(errMsg);
    }

    let useAltClient = false;
    if (
      !(forceNetwork == undefined) &&
      forceNetwork.length &&
      network !== forceNetwork
    ) {
      network = forceNetwork;
      useAltClient = true;
    }

    const buildClient = new ClientBuilder();
    if (network === 'testnet') {
      // buildClient.node('https://api.lb-0.testnet.chrysalis2.com');
      buildClient.node('https://api.lb-0.h.chrysalis-devnet.iota.cafe');
    } else {
      // setting network seems redundant
      // .network(network)
      buildClient.node('https://chrysalis-nodes.iota.org');
    }
    let client: Client;
    if (useAltClient) {
      if (this.altIotaClient == undefined) {
        this.altIotaClient = buildClient.build();
        this.altIotaClient
          .getInfo()
          .then((result) => {
            this.logger.info('using network: ' + network);
            this.logger.info(result);
          })
          .catch((err) => this.logger.error(err));
      }
      client = this.altIotaClient;
    } else {
      if (this.iotaClient == undefined) {
        this.iotaClient = buildClient.build();
        this.iotaClient
          .getInfo()
          .then((result) => {
            this.logger.info('using network: ' + network);
            this.logger.info(result);
          })
          .catch((err) => this.logger.error(err));
      }
      client = this.iotaClient;
    }
    return client;
  }

  /**
   *
   *
   * @param {number} balance
   * @param {string} collector
   * @param {string} recipient
   * @param {string} seedEncryptionPw
   * @param {string} encryptedSeed
   * @param {string} forceNetwork
   * @return {*}  {Promise<any>}
   * @memberof WalletService
   */
  async recoverTokensToCollectorAddress(
    balance: number,
    collector: string,
    recipient: string,
    seedEncryptionPw: string,
    encryptedSeed: string,
    forceNetwork: string
  ): Promise<any> {
    return new Promise(async (resolve, reject) => {
      const client = this.getWalletClient(forceNetwork);
      const seed = decrypt(encryptedSeed, seedEncryptionPw);

      const message = await client
        .message()
        .seed(seed)
        .output(collector, parseFloat(balance as any))
        .submit()
        .catch((err) => {
          reject(err);
        });

      return resolve(message);
    });
  }

  /**
   *
   *
   * @param {Address} addressObj
   * @return {*}  {Promise<any>}
   * @memberof WalletService
   */
  async sendToCollectorAddress(addressObj: Address): Promise<any> {
    return new Promise(async (resolve, reject) => {
      const client = this.getWalletClient();
      const collectorAddress = this.configService.getValue('COLLECTOR_ADDRESS');
      if (collectorAddress == undefined || !collectorAddress.length) {
        return reject(new Error('COLLECTOR_ADDRESS was not set'));
      }
      if (addressObj == undefined || addressObj.address == undefined) {
        return reject(new Error('address was not provided'));
      }

      const qb = this.walletRepository.createQueryBuilder('wallet');
      qb.leftJoinAndSelect('wallet.accounts', 'accounts');
      qb.leftJoinAndSelect('accounts.addresses', 'addresses');
      qb.where('addresses.address = :address', {
        address: addressObj.address,
      });
      qb.select([
        'wallet.walletid',
        'wallet.seed',
        'accounts.accountid',
        'addresses.address',
      ]);
      const walletObj = (await qb.getOne().catch((err) => {
        reject(err);
      })) as Wallet;
      if (walletObj == undefined) {
        return reject(new Error('no wallet found'));
      }

      const encryptedSeed = walletObj.seed;
      if (encryptedSeed == undefined || !encryptedSeed.length) {
        return reject(new Error('no seed found'));
      }
      const seed = decrypt(encryptedSeed);

      const balance = await client
        .getBalance(seed)
        .get()
        .catch((err) => {
          reject(err);
        });
      if (balance == undefined || balance <= 0) {
        return reject(
          new Error('no balance available in address ' + addressObj.address)
        );
      }

      let message = await client
        .message()
        .seed(seed)
        .dustAllowanceOutput(collectorAddress, parseFloat(balance as any))
        .submit()
        .catch((err) => {
          this.logger.error(err, this.loggerContext);
        });

      if (message == undefined) {
        // if dustAllowanceOutput is not working, retry
        message = await client
          .message()
          .seed(seed)
          .output(collectorAddress, parseFloat(balance as any))
          .submit()
          .catch((err) => {
            reject(err);
          });
      }
      if (message == undefined) {
        this.logger.error('transaction failed', this.loggerContext);
        return reject(new Error('transaction failed'));
      }

      return resolve({ message, balance });
    });
  }

  /**
   *
   *
   * @param {Address} addressObj
   * @return {*}  {Promise<any>}
   * @memberof WalletService
   */
  async getTransactionDetails(addressObj: Address): Promise<any> {
    return new Promise(async (resolve, reject) => {
      const client = this.getWalletClient();

      const outputs = (await client
        .getAddressOutputs(addressObj.address, { includeSpent: true })
        .catch((err) => {
          reject(err);
        })) as string[];
      if (outputs == undefined) {
        return reject('no outputs found');
      }

      const details = [];
      for (const entry of outputs) {
        const output = (await client
          .getOutput(entry)
          .catch((err) => this.logger.error(err, this.loggerContext))) as any;
        if (output == undefined) {
          continue;
        }

        const data = (await client
          .getMessage()
          .data(output.messageId)
          .catch((err) => this.logger.error(err, this.loggerContext))) as any;
        if (data == undefined) {
          continue;
        }

        details.push({ output, data });
      }

      return resolve(JSON.stringify(details));
    });
  }
}

import { Injectable } from '@nestjs/common';
import { GlobalConfigService } from './config.service';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { LoggingService } from './logging.service';
import { WalletService } from './wallet.service';
import { Address } from '../../entities/address.entity';
import { Report } from '../../entities/report.entity';
import axios from 'axios';

@Injectable()
export class WebhookService {
  loggerContext = 'WebhookService';

  constructor(
    @InjectRepository(Report)
    private readonly reportRepository: Repository<Report>,
    private readonly configService: GlobalConfigService,
    private readonly logger: LoggingService,
    private readonly walletService: WalletService
  ) {}

  /**
   *
   *
   * @param {*} data
   * @return {*}  {Promise<any>}
   * @memberof WebhookService
   */
  async sendDataValidationWebhook(data: any): Promise<any> {
    return new Promise(async (resolve, reject) => {
      const dataValidationUrl = this.configService.getValue(
        'TX_DATA_VALIDATION_WEBHOOK',
        false
      );
      const webhookSecret = this.configService.getValue(
        'WEBHOOK_SECRET',
        false
      );

      if (!(dataValidationUrl == undefined) && dataValidationUrl.length) {
        let errorState: any;
        const validationResult = (await axios
          .get(dataValidationUrl, {
            params: {
              type: 'validation',
              data,
              secret: webhookSecret,
            },
            timeout: 10000,
          })
          .catch((err) => {
            errorState = err;
            this.logger.error(err + ' data validation failed', this.loggerContext);
          })) as any;
        if (validationResult == undefined || !(errorState == undefined)) {
          return reject(errorState || new Error('data validation failed'));
        }
        if (
          !(validationResult.data == undefined) &&
          !(validationResult.data.data == undefined)
        ) {
          data = validationResult.data.data;
        } else if (!(validationResult.data == undefined)) {
          data = validationResult.data;
        } else {
          return reject(new Error('data validation failed'));
        }
      }
      return resolve(data);
    });
  }

  /**
   *
   *
   * @param {Address} addressObj
   * @return {*}  {Promise<void>}
   * @memberof WebhookService
   */
  async sendStartWebhook(addressObj: Address): Promise<void> {
    return new Promise(async (resolve) => {
      const startTxWebhook = this.configService.getValue(
        'TX_START_WEBHOOK',
        false
      );
      const webhookSecret = this.configService.getValue(
        'WEBHOOK_SECRET',
        false
      );
      if (!(startTxWebhook == undefined) && startTxWebhook.length) {
        axios
          .get(startTxWebhook, {
            params: {
              type: 'start',
              address: addressObj.address,
              data: addressObj.data,
              secret: webhookSecret,
            },
            timeout: 10000,
          })
          .catch((err) => this.logger.error(err, this.loggerContext));
      }
      return resolve();
    });
  }

  /**
   *
   *
   * @param {Address} addressObj
   * @return {*}  {Promise<void>}
   * @memberof WebhookService
   */
  async sendCompleteWebhook(addressObj: Address): Promise<void> {
    return new Promise(async (resolve) => {
      const completeTxWebhook = this.configService.getValue(
        'TX_COMPLETE_WEBHOOK',
        false
      );
      const webhookSecret = this.configService.getValue(
        'WEBHOOK_SECRET',
        false
      );
      if (!(completeTxWebhook == undefined) && completeTxWebhook.length) {
        axios
          .get(completeTxWebhook, {
            params: {
              type: 'complete',
              address: addressObj.address,
              balance: addressObj.balance,
              data: addressObj.data,
              resolvedAt: new Date(
                addressObj.resolvedAt || new Date()
              ).toISOString(),
              secret: webhookSecret,
            },
            timeout: 10000,
          })
          .catch((err) => this.logger.error(err, this.loggerContext));
      }
      return resolve();
    });
  }

  /**
   *
   *
   * @param {Address} addressObj
   * @return {*}  {Promise<void>}
   * @memberof WebhookService
   */
  async sendPartialWebhook(addressObj: Address): Promise<void> {
    return new Promise(async (resolve) => {
      const partialTxWebhook = this.configService.getValue(
        'TX_PARTIAL_WEBHOOK',
        false
      );
      const webhookSecret = this.configService.getValue(
        'WEBHOOK_SECRET',
        false
      );
      if (!(partialTxWebhook == undefined) && partialTxWebhook.length) {
        axios
          .get(partialTxWebhook, {
            params: {
              type: 'partial',
              address: addressObj.address,
              balance: addressObj.lastReportedBalance,
              data: addressObj.data,
              secret: webhookSecret,
            },
            timeout: 10000,
          })
          .catch((err) => this.logger.error(err, this.loggerContext));
      }
      return resolve();
    });
  }

  /**
   *
   *
   * @param {Address} addressObj
   * @return {*}  {Promise<void>}
   * @memberof WebhookService
   */
  async sendCanceledWebhook(addressObj: Address): Promise<void> {
    return new Promise(async (resolve) => {
      const canceledTxWebhook = this.configService.getValue(
        'TX_CANCELED_WEBHOOK',
        false
      );
      const webhookSecret = this.configService.getValue(
        'WEBHOOK_SECRET',
        false
      );
      if (!(canceledTxWebhook == undefined) && canceledTxWebhook.length) {
        axios
          .get(canceledTxWebhook, {
            params: {
              type: 'canceled',
              address: addressObj.address,
              balance: addressObj.balance,
              data: addressObj.data,
              secret: webhookSecret,
            },
            timeout: 10000,
          })
          .catch((err) => this.logger.error(err, this.loggerContext));
      }
      return resolve();
    });
  }

  /**
   *
   *
   * @param {Address} addressObj
   * @return {*}  {Promise<void>}
   * @memberof WebhookService
   */
  async sendReportWebhook(addressObj: Address): Promise<Address> {
    return new Promise(async (resolve) => {
      if (addressObj.reportedAt == undefined) {
        const transactionDetails = await this.walletService
          .getTransactionDetails(addressObj)
          .catch((err) => this.logger.error(err));
        if (transactionDetails == undefined) {
          return resolve(undefined);
        }

        const newReportObj = new Report().setAttr({
          address: addressObj.address,
          balance: addressObj.balance,
          data: addressObj.data,
          transactionDetails,
          resolvedAt: addressObj.resolvedAt,
        });
        const reportInsertResult = await this.reportRepository
          .save(newReportObj)
          .catch((err) => this.logger.error(err));
        if (reportInsertResult == undefined) {
          return resolve(undefined);
        }

        const reportTxWebhook = this.configService.getValue(
          'TX_REPORT_WEBHOOK',
          false
        );
        const webhookSecret = this.configService.getValue(
          'WEBHOOK_SECRET',
          false
        );
        if (!(reportTxWebhook == undefined) && reportTxWebhook.length) {
          axios
            .post(reportTxWebhook, reportInsertResult, {
              params: {
                type: 'report',
                address: addressObj.address,
                secret: webhookSecret,
              },
              timeout: 10000,
            })
            .catch((err) => this.logger.error(err, this.loggerContext));
        }

        addressObj.reportedAt = new Date();
      }
      return resolve(addressObj);
    });
  }
}

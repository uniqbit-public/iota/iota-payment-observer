import { Injectable, Logger } from '@nestjs/common';
import { GlobalConfigService } from './config.service';

@Injectable()
export class LoggingService {
  logLevel =
    this.configService.getValue('LOG_LEVEL', false) || 'error,warn,info,debug';

  constructor(
    private readonly configService: GlobalConfigService,
    private readonly logger: Logger
  ) {}

  error(message: any, context?: string, trace?: string) {
    if (this.logLevel.indexOf('error') > -1) {
      this.logger.error(message, trace, context);
    }
  }

  warn(message: any, context?: string) {
    if (this.logLevel.indexOf('warn') > -1) {
      this.logger.warn(message, context);
    }
  }

  info(message: any, context?: string) {
    if (this.logLevel.indexOf('info') > -1) {
      this.logger.log(message, context);
    }
  }

  log(message: any, context?: string) {
    if (this.logLevel.indexOf('info') > -1) {
      this.logger.log(message, context);
    }
  }

  debug(message: any, context?: string) {
    if (this.logLevel.indexOf('debug') > -1) {
      this.logger.debug(message, context);
    }
  }
}

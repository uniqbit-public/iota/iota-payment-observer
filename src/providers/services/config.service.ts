import { Injectable, Logger } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';

export const envVars = {
  ENV: process.env.ENV || 'DEV',
  ENV_MODE: process.env.ENV || 'DEV',
  LOG_LEVEL: process.env.LOG_LEVEL || 'error,warn,info,debug',
  APP_PORT: process.env.APP_PORT || '4040',
  POSTGRES_HOST: process.env.POSTGRES_HOST,
  POSTGRES_USER: process.env.POSTGRES_USER,
  POSTGRES_PORT: process.env.POSTGRES_PORT,
  POSTGRES_PASSWORD: process.env.POSTGRES_PASSWORD,
  POSTGRES_DB: process.env.POSTGRES_DB,
  IOTA_NETWORK: process.env.IOTA_NETWORK,
  SEED_ENCRYPTION_PW: process.env.SEED_ENCRYPTION_PW,
  COLLECTOR_ADDRESS: process.env.COLLECTOR_ADDRESS,
  /**
   * We do not require following vars, when commented in, 
   * the ensure vars function will throw an error.
   * It's only left in hear for overview purposes
   */ 
  // TX_START_WEBHOOK: process.env.TX_START_WEBHOOK,
  // TX_COMPLETE_WEBHOOK: process.env.TX_COMPLETE_WEBHOOK,
  // TX_CANCELED_WEBHOOK: process.env.TX_CANCELED_WEBHOOK,
  // TX_DATA_VALIDATION_WEBHOOK: process.env.TX_DATA_VALIDATION_WEBHOOK,
  // TX_PARTIAL_WEBHOOK: process.env.TX_PARTIAL_WEBHOOK,
  // TX_REPORT_WEBHOOK: process.env.TX_REPORT_WEBHOOK,
  // DATA_POLICY_URL: process.env.DATA_POLICY_URL,
  // LEGAL_NOTICE_URL: process.env.LEGAL_NOTICE_URL,
};

@Injectable()
export class GlobalConfigService {
  public cachedValidTenantIds: string[];

  private namespace = 'src/services/config.service.ts';
  // prevent circular dependency
  private loggerInst: Logger;
  get logger(): Logger {
    if (!this.loggerInst) {
      this.loggerInst = new Logger(GlobalConfigService.name);
    }
    return this.loggerInst;
  }

  static get currentEnv(): string {
    return String(process.env.ENV || 'DEV')
      .toUpperCase()
      .trim();
  }

  constructor(
    private readonly configService: ConfigService
  ) {
    this.logger.log(`Loaded the config: --> .env`, this.namespace);
    this.ensureValues(Object.keys(envVars));
  }

  public getValue(key: string, throwOnMissing = true): string {
    const value = envVars[key] || this.configService.get(key);
    if (!value && throwOnMissing) {
      throw new Error(`config error - missing env.${key}`);
    }

    return value;
  }

  /**
   * API should fail hard, if requested variable is not available in .env.*
   * or ENV variables/secrets from CI pipeline (kubernetes ENV)
   */
  public ensureValues(keys: string[]): void {
    keys.forEach((key) => {
      if (!envVars[key] && !this.getValue(key, true)) {
        const msg =
          'Missing envirnoment variable, check your .env file and gitlab CI vars!';
        if (this.logger) this.logger.error(this.namespace, msg);
        // eslint-disable-next-line no-console
        else console.error('logger is undefined', this.namespace, msg);
        throw new Error(msg);
      }
    });
  }

  public getPort(): string {
    const key = 'APP_PORT';
    return envVars[key] || this.getValue(key, true);
  }

  public isProduction(): boolean {
    return envVars['ENV'] === 'PROD';
  }

  public isDev(): boolean {
    return envVars['ENV'] === 'DEV';
  }

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  public getLogLevel(): any {
    const key = 'LOG_LEVEL';
    const values = envVars[key] || this.getValue(key, true);
    const retObj = {};
    if (values)
      values.split(',').forEach((value: string | number) => {
        retObj[value] = true;
      });
    return retObj;
  }
}

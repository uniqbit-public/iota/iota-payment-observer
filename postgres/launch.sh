#!/bin/sh

# source env vars
set -o allexport
. ./.env.dev
set +o allexport

if [ -z "$(docker volume ls -q -f "name=$PSQL_VOLUME_NAME")" ]; then
  docker volume create $PSQL_VOLUME_NAME
fi

# override localhost
POSTGRES_HOST=ipodb

docker network create --driver bridge ipo_net || true

docker rm -f $(docker ps -a -q --filter="name=$POSTGRES_HOST")
docker run --rm --name $POSTGRES_HOST -d -p $POSTGRES_PORT:5432 \
            --network=ipo_net \
            -v $PSQL_VOLUME_NAME:/var/lib/postgresql/data \
            -e POSTGRES_USER \
            -e POSTGRES_PASSWORD \
            -e POSTGRES_DB \
            postgres:12-alpine

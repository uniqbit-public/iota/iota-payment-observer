#!/bin/sh

# source env vars
set -o allexport
. ./.env.dev
set +o allexport

# reset volume
docker rm -f $(docker ps -a -q --filter="name=$POSTGRES_HOST")
docker volume rm $PSQL_VOLUME_NAME

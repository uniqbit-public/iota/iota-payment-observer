import { Component } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { IOTAService } from '../@core/services/iota/iota.service';
import { RoutingService } from '../@core/services/routing/routing.service';
import { ActivatedRoute } from '@angular/router';
import { debounce } from '../@core/libraries/basic/debounce.lib';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {
  maxWindowOpenFallbackTime = 1000 * 60 * 60 * 1;

  constructor(
    private readonly iotaService: IOTAService,
    private readonly routing: RoutingService,
    private readonly actRoute: ActivatedRoute,
    private readonly translateService: TranslateService
  ) {
    window.addEventListener('beforeunload', () => {
      this.iotaService.windowClose();
    });

    // receive event from parent website via e. g.
    // document.getElementById('ipo').contentWindow.postMessage('done', '*'));
    // set success state without sending cancel to api upon close
    window.addEventListener('done', () => {
      this.iotaService.recieverAddress.isSuccess = true;
      this.routing.navigate(['/success'], {
        queryParams: this.actRoute.snapshot.queryParams
      });
    });
    window.addEventListener('message', (event: any) => {
      if (event.data === 'done') {
        this.iotaService.recieverAddress.isSuccess = true;
        this.routing.navigate(['/success'], {
          queryParams: this.actRoute.snapshot.queryParams
        });
      } else if (event.data === 'close') {
        this.iotaService.windowClose();
      }
    });

    debounce('maxTransactionTimeout', this.maxWindowOpenFallbackTime, () => {
      this.routing.data = new Error(this.translateService.instant('FAIL.EXPIRED'));
      this.routing.navigate(['/fail']);
    });
  }
}

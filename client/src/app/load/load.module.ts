import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { LoadComponent } from './load.component';

import { LoadRoutingModule } from './load-routing.module';

const MODULES = [CommonModule, IonicModule, LoadRoutingModule];

const COMPONENTS = [LoadComponent];

@NgModule({
  imports: [...MODULES],
  declarations: [...COMPONENTS]
})
export class LoadModule {}

import { Component, OnInit } from '@angular/core';
import { debounce } from '../../@core/libraries/basic/debounce.lib';
import { IOTAService } from '../../@core/services/iota/iota.service';
import { RoutingService } from '../../@core/services/routing/routing.service';
import { ThemeService } from '../../@core/services/theme/theme.service';
import { LanguageService } from '../../@core/services/language/language.service';
import { WindowEventService } from '../../@core/services/window-event/window-event.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-load',
  templateUrl: 'load.component.html',
  styleUrls: ['load.component.scss']
})
export class LoadComponent implements OnInit {
  isPullingNewAddress = false;

  constructor(
    private readonly themeService: ThemeService,
    private readonly languageService: LanguageService,
    private readonly iotaService: IOTAService,
    private readonly actRoute: ActivatedRoute,
    private readonly routing: RoutingService,
    private readonly windowEventService: WindowEventService
  ) {}

  ngOnInit() {
    const theme = this.actRoute.snapshot.queryParams.theme;
    if (!(theme == undefined)) {
      this.themeService.switchTheme(theme);
    }

    const lang = this.actRoute.snapshot.queryParams.lang;
    if (!(lang == undefined)) {
      this.languageService.switchLanguage(lang);
    }

    // data can be for example a base64 encoded stringified json object
    const data = this.actRoute.snapshot.queryParams.data;
    if (!this.isPullingNewAddress) {
      // debounce possible duplicate requests
      debounce('initGetAddress', 25, async () => {
        this.isPullingNewAddress = true;
        await this.iotaService
          .getNewAddress(data)
          .then(() => {
            this.routing.navigate(['/transaction'], {
              queryParams: this.actRoute.snapshot.queryParams
            });
          })
          .catch(err => {
            this.routing.data = err;
            this.routing.navigate(['/fail'], {
              queryParams: this.actRoute.snapshot.queryParams
            });
          });
        this.isPullingNewAddress = false;
      });
    }

    if (!(this.actRoute.snapshot.queryParams.id == undefined)) {
      this.windowEventService.id = this.actRoute.snapshot.queryParams.id;
    }
  }
}

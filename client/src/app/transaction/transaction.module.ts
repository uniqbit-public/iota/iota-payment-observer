import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { TranslateModule } from '@ngx-translate/core';
import { FooterModule } from '../../@theme/components/footer/footer.module';
import { QrCodeModule } from '../../@core/services/qr-code/qr-code.module';
import { TransactionComponent } from './transaction.component';

import { TransactionRoutingModule } from './transaction-routing.module';

const MODULES = [
  CommonModule,
  IonicModule,
  TranslateModule,
  TransactionRoutingModule,
  QrCodeModule,
  FooterModule
];

const COMPONENTS = [TransactionComponent];

@NgModule({
  imports: [...MODULES],
  declarations: [...COMPONENTS]
})
export class TransactionModule {}

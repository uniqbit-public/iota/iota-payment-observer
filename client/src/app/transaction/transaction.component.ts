import { Component, OnInit, OnDestroy } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { debounce } from '../../@core/libraries/basic/debounce.lib';
import { IOTAService } from '../../@core/services/iota/iota.service';
import { RoutingService } from '../../@core/services/routing/routing.service';
import { NotificationService } from '../../@core/services/notification/notification.service';
import { WindowEventService } from '../../@core/services/window-event/window-event.service';
import { ActivatedRoute } from '@angular/router';
import * as Clipboard from 'clipboard';
import { environment } from '../../environments/environment';

@Component({
  selector: 'app-transaction',
  templateUrl: 'transaction.component.html',
  styleUrls: ['transaction.component.scss']
})
export class TransactionComponent implements OnInit, OnDestroy {
  clipboard: Clipboard;
  isSuccess = false;

  miotaMulti = 1000000;
  miotaDivider = 0.000001;
  miotaSymbol = 'Mi';

  get address(): string {
    if (!(this.iotaService.recieverAddress == undefined)) {
      return this.iotaService.recieverAddress.address;
    }
  }

  get data(): any {
    if (
      !(this.iotaService.recieverAddress == undefined) &&
      !(this.iotaService.recieverAddress.data == undefined)
    ) {
      let data = this.iotaService.recieverAddress.data;
      if (
        typeof this.iotaService.recieverAddress.data === 'string' &&
        this.iotaService.recieverAddress.data.length
      ) {
        try {
          data = JSON.parse(window.atob(this.iotaService.recieverAddress.data));
        } catch (e) {
          console.error(e);
        }
      }
      return data;
    }
    return this.iotaService.data;
  }

  get symbol(): string {
    if (this.actRoute.snapshot.queryParams.symbol == undefined) {
      return 'USD';
    }
    return this.actRoute.snapshot.queryParams.symbol;
  }

  get receivedPrice(): number {
    if (
      this.balance == undefined ||
      this.iotaService.currentIOTAPrice == undefined
    ) {
      return;
    }
    return this.balance * parseFloat(this.iotaService.currentIOTAPrice as any);
  }

  get expectedPrice(): number {
    if (
      this.expectedBalance == undefined ||
      this.iotaService.currentIOTAPrice == undefined
    ) {
      return;
    }
    if (this.expectedBalance > -1) {
      return (
        this.expectedBalance *
        parseFloat(this.iotaService.currentIOTAPrice as any)
      );
    }
  }

  get expectedValue(): number {
    if (this.data == undefined || this.data.value == undefined) {
      return -1;
    }
    return parseFloat(this.data.value);
  }

  // balance in Mi
  get expectedBalance(): number {
    if (this.expectedValue == undefined || this.expectedValue === -1) {
      return -1;
    }
    return this.expectedValue * this.miotaDivider * 0.999; // reduce expected value by 0.1% to account for inaccuracies
  }

  // balance in Mi
  get balance(): number {
    if (!(this.iotaService.recieverAddress == undefined)) {
      const val = parseFloat(this.iotaService.recieverAddress.balance || 0);
      return val > 0 ? val * this.miotaDivider : 0;
    }
    return 0;
  }

  constructor(
    private readonly iotaService: IOTAService,
    private readonly actRoute: ActivatedRoute,
    private readonly routing: RoutingService,
    private readonly note: NotificationService,
    private readonly windowEventService: WindowEventService,
    private readonly translateService: TranslateService
  ) {}

  ngOnInit() {
    this.iotaService
      .getCurrentIOTAPrice(this.actRoute.snapshot.queryParams.symbol || 'USD')
      .then(priceObj => {
        this.iotaService.currentIOTAPrice = priceObj.price
          ? parseFloat(priceObj.price)
          : undefined;

        // TESTING
        // console.info('getCurrentIOTAPrice', priceObj);
      })
      .catch(err => {
        console.error(err);
      });

    this.onInit();
  }

  ionViewDidEnter() {
    this.onInit();
  }

  onInit() {
    if (this.iotaService.recieverAddress == undefined) {
      this.routing.navigate(['/load'], {
        queryParams: this.actRoute.snapshot.queryParams
      });
    } else {
      this.registerClipboardEvents();
      this.pullBalanceUpdate();
    }

    if (!(this.actRoute.snapshot.queryParams.id == undefined)) {
      this.windowEventService.id = this.actRoute.snapshot.queryParams.id;
    }
  }

  pullBalanceUpdate() {
    if (this.iotaService.isClosing) {
      return;
    }
    debounce('pullBalanceUpdate', 1000, async () => {
      if (this.iotaService.isClosing) {
        return;
      }
      if (
        !this.isSuccess &&
        !(this.iotaService.recieverAddress == undefined) &&
        !(this.iotaService.recieverAddress.address == undefined) &&
        !this.iotaService.recieverAddress.isSuccess &&
        !this.iotaService.recieverAddress.isCancel &&
        window.location.href.indexOf('/#/transaction') > -1
      ) {
        await this.iotaService
          .getAddressBalance(this.address)
          .then(result => {
            if (result.isCancel) {
              this.routing.data = new Error(
                this.translateService.instant('PAYMENT.WAS_CANCELED')
              );
              this.routing.navigate(['/fail']);
            }

            const newVal = parseFloat(result.balance || 0);
            const currentBalancVal = parseFloat(
              this.iotaService.recieverAddress.balance || 0
            );

            if (newVal !== currentBalancVal) {
              this.iotaService.recieverAddress.balance = newVal;

              if (this.expectedValue === -1 && newVal > 0) {
                this.isSuccess = true;
              } else if (newVal >= this.expectedValue) {
                this.isSuccess = true;
              }

              if (this.isSuccess) {
                debounce('receivedBalanceUpdate', 2000, () => {
                  this.routing.navigate(['/success'], {
                    queryParams: this.actRoute.snapshot.queryParams
                  });
                });
              }
            }
          })
          .catch(err => console.error(err));
        this.pullBalanceUpdate();
      }
    });
  }

  registerClipboardEvents() {
    this.clipboard = new Clipboard('.clipboard', {
      text: () => `${this.address}`
    });
    this.clipboard.on('success', e => success(e, this.note));
    this.clipboard.on('error', e => fail(e, this.note));
    const success = (e: any, note: NotificationService) => {
      note.showNotification('info', null, 'TOASTER.COPIED_TO_CLIPBOARD');
      e.clearSelection();
    };
    const fail = (e: any, note: NotificationService) => {
      note.showNotification('error', null, 'TOASTER.CLIPBOARD_FAILED');
      e.clearSelection();
    };
  }

  onIOTAFoundation() {
    window.open(environment.iotaFoundationUrl, '_tab', undefined);
  }

  onGitLabProject() {
    window.open(environment.gitlabProjectUrl, '_tab', undefined);
  }

  ngOnDestroy() {
    if (!(this.clipboard == undefined)) {
      this.clipboard.destroy();
    }
  }
}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { TranslateModule } from '@ngx-translate/core';
import { FooterModule } from '../../@theme/components/footer/footer.module';
import { FailComponent } from './fail.component';

import { FailRoutingModule } from './fail-routing.module';

const MODULES = [
  CommonModule,
  IonicModule,
  TranslateModule,
  FailRoutingModule,
  FooterModule,
];

const COMPONENTS = [FailComponent];

@NgModule({
  imports: [...MODULES],
  declarations: [...COMPONENTS]
})
export class FailModule {}

import { Component } from '@angular/core';
import { IOTAService } from '../../@core/services/iota/iota.service';
import { RoutingService } from '../../@core/services/routing/routing.service';
import { WindowEventService } from '../../@core/services/window-event/window-event.service';

@Component({
  selector: 'app-fail',
  templateUrl: 'fail.component.html',
  styleUrls: ['fail.component.scss']
})
export class FailComponent {
  get address(): string {
    if (!(this.iotaService.recieverAddress == undefined)) {
      return this.iotaService.recieverAddress.address;
    }
  }

  get errorMessage(): string {
    if (this.routing.data == undefined) {
      return '';
    }
    if (
      !(this.routing.data.error == undefined) &&
      !(this.routing.data.error.message == undefined)
    ) {
      return this.routing.data.error.message;
    } else if (!(this.routing.data.error == undefined)) {
      return this.routing.data.error;
    }
    return this.routing.data.message;
  }

  constructor(
    private readonly iotaService: IOTAService,
    private readonly windowEventService: WindowEventService,
    private readonly routing: RoutingService
  ) {}

  onClose() {
    this.windowEventService.emitMessage(
      'cancel',
      this.iotaService.recieverAddress
    );
    this.iotaService.windowClose();
    window.close();
  }
}

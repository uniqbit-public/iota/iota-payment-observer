import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DataPolicyComponent } from './data/data-policy.component';
import { LegalNoticeComponent } from './notice/legal-notice.component';

const routes: Routes = [
  {
    path: 'data',
    component: DataPolicyComponent
  },
  {
    path: 'notice',
    component: LegalNoticeComponent
  },
  {
    path: '',
    redirectTo: 'notice',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LegalRoutingModule {}

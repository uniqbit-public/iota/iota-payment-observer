import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { DataPolicyComponent } from './data/data-policy.component';
import { LegalNoticeComponent } from './notice/legal-notice.component';

import { LegalRoutingModule } from './legal-routing.module';

const MODULES = [CommonModule, IonicModule, LegalRoutingModule];

const COMPONENTS = [DataPolicyComponent, LegalNoticeComponent];

@NgModule({
  imports: [...MODULES],
  declarations: [...COMPONENTS]
})
export class LegalModule {}

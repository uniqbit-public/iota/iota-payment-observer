import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { TranslateModule } from '@ngx-translate/core';
import { FooterModule } from '../../@theme/components/footer/footer.module';
import { SuccessComponent } from './success.component';

import { SuccessRoutingModule } from './success-routing.module';

const MODULES = [
  CommonModule,
  IonicModule,
  TranslateModule,
  SuccessRoutingModule,
  FooterModule,
];

const COMPONENTS = [SuccessComponent];

@NgModule({
  imports: [...MODULES],
  declarations: [...COMPONENTS]
})
export class SuccessModule {}

import { Component, OnInit } from '@angular/core';
import { IOTAService } from '../../@core/services/iota/iota.service';
import { RoutingService } from '../../@core/services/routing/routing.service';
import { WindowEventService } from '../../@core/services/window-event/window-event.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-success',
  templateUrl: 'success.component.html',
  styleUrls: ['success.component.scss']
})
export class SuccessComponent implements OnInit {
  get address(): string {
    if (!(this.iotaService.recieverAddress == undefined)) {
      return this.iotaService.recieverAddress.address;
    }
  }

  constructor(
    private readonly iotaService: IOTAService,
    private readonly actRoute: ActivatedRoute,
    private readonly routing: RoutingService,
    private readonly windowEventService: WindowEventService
  ) {}

  ngOnInit() {
    this.onInit();
  }

  ionViewDidEnter() {
    this.onInit();
  }

  onInit() {
    if (this.iotaService.recieverAddress == undefined) {
      this.routing.navigate(['/load'], {
        queryParams: this.actRoute.snapshot.queryParams
      });
    } else {
      this.iotaService.recieverAddress.isSuccess = true;
      this.windowEventService.emitMessage('success', this.iotaService.recieverAddress);
    }

    if (!(this.actRoute.snapshot.queryParams.id == undefined)) {
      this.windowEventService.id = this.actRoute.snapshot.queryParams.id;
    }
  }

  onClose() {
    this.iotaService.recieverAddress.isSuccess = true;
    this.iotaService.windowClose();
    window.close();
  }
}

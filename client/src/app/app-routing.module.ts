import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'load',
    loadChildren: () => import('./load/load.module').then(m => m.LoadModule)
  },
  {
    path: 'transaction',
    loadChildren: () =>
      import('./transaction/transaction.module').then(m => m.TransactionModule)
  },
  {
    path: 'success',
    loadChildren: () =>
      import('./success/success.module').then(m => m.SuccessModule)
  },
  {
    path: 'fail',
    loadChildren: () => import('./fail/fail.module').then(m => m.FailModule)
  },
  {
    path: 'legal',
    loadChildren: () => import('./legal/legal.module').then(m => m.LegalModule)
  },
  {
    path: '',
    redirectTo: 'load',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}

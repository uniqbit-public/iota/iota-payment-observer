import { NgModule } from '@angular/core';
import { HashLocationStrategy, LocationStrategy } from '@angular/common';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';
import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { CoreModule } from '../@core/@core.module';

// required for AOT compilation
export const createTranslateLoader = (http: HttpClient) => {
  return new TranslateHttpLoader(http, 'assets/language/', '.json');
};

const MODULES = [
  BrowserModule,
  IonicModule.forRoot(),
  AppRoutingModule,
  CoreModule,
  HttpClientModule,
  TranslateModule.forRoot({
    defaultLanguage: 'en',
    loader: {
      provide: TranslateLoader,
      useFactory: createTranslateLoader,
      deps: [HttpClient]
    }
  }),
];

const PROVIDERS = [
  { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
  { provide: LocationStrategy, useClass: HashLocationStrategy }
];

const COMPONENTS = [AppComponent];

@NgModule({
  declarations: [...COMPONENTS],
  entryComponents: [],
  imports: [...MODULES],
  providers: [...PROVIDERS],
  bootstrap: [...COMPONENTS]
})
export class AppModule {}

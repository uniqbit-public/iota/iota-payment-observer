import { Component, OnInit } from '@angular/core';
import { LegalService } from '../../../@core/services/legal/legal.service';

@Component({
  selector: 'app-footer',
  templateUrl: 'footer.component.html',
  styleUrls: ['footer.component.scss']
})
export class FooterComponent implements OnInit {
  constructor(
    public readonly legalService: LegalService
  ) {}

  ngOnInit() {
    this.legalService.fetchLegalLinks();
  }

  onDataPolicy() {
    if (!(this.legalService.dataPolicyUrl == undefined)) {
      window.open(this.legalService.dataPolicyUrl, '_tab');
    }
  }

  onLegalNotice() {
    if (!(this.legalService.legalNoticeUrl == undefined)) {
      window.open(this.legalService.legalNoticeUrl, '_tab');
    }
  }
}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { IonicModule } from '@ionic/angular';
import { FooterComponent } from './footer.component';

const MODULES = [CommonModule, IonicModule, TranslateModule];

const COMPONENTS = [FooterComponent];

@NgModule({
  imports: [...MODULES],
  declarations: [...COMPONENTS],
  exports: [...COMPONENTS]
})
export class FooterModule {}

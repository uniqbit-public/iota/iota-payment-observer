export const environment = {
  production: true,
  gitlabProjectUrl: 'https://gitlab.com/uniqbit-public/iota/iota-payment-observer',
  iotaFoundationUrl: 'https://www.iota.org/'
};

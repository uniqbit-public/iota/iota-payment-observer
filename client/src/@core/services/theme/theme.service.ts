import { Injectable } from '@angular/core';
import { sleep } from '../../libraries/basic/sleep.lib';

export interface Theme {
  name: string;
  theme: string;
  header: string;
  code: string;
  icon: string;
  hidden?: boolean;
}

@Injectable()
export class ThemeService {
  availableThemes: Theme[] = [
    {
      name: 'default',
      theme: 'dark',
      header: 'light',
      code: 'THEME.DEFAULT',
      icon: 'contrast-outline',
      hidden: true
    },
    {
      name: 'dark',
      theme: 'dark',
      header: 'light',
      code: 'THEME.DARK',
      icon: 'moon-outline'
    },
    {
      name: 'light',
      theme: 'light',
      header: 'light',
      code: 'THEME.LIGHT',
      icon: 'sunny-outline'
    }
  ];

  activeTheme: Theme = this.availableThemes[1];

  get currentTheme(): Theme {
    return this.activeTheme;
  }

  async applyActiveTheme(breakCounter?: number) {
    const bodyElement = document.getElementById('main-body');
    if (!(bodyElement == undefined)) {
      bodyElement.setAttribute('class', this.activeTheme.name);
    } else {
      breakCounter = breakCounter || 0 + 1;
      if (breakCounter < 10) {
        await sleep(10);
        this.applyActiveTheme(breakCounter);
      }
    }
  }

  switchTheme(theme?: string) {
    let found = false;
    if (!(theme == undefined)) {
      for (const entry of this.availableThemes) {
        if (entry.name === theme) {
          found = true;
          this.activeTheme = entry;
          break;
        }
      }
    }
    if (!found) {
      this.activeTheme = this.availableThemes[0];
    }
    this.applyActiveTheme();
  }
}

import { Injectable } from '@angular/core';

@Injectable()
export class WindowEventService {
  id: string;

  // states
  success = false;
  cancel = false;
  closing = false;

  emitMessage(eventName: string, data: any) {
    if (!this[eventName]) {
      window.dispatchEvent(
        new CustomEvent(eventName, {
          detail: { payload: data, id: this.id }
        })
      );
      if (
        !(window.opener == undefined) &&
        !(window.opener.postMessage == undefined)
      ) {
        // emit event cross domain
        window.opener.postMessage(
          { type: eventName, payload: data, id: this.id },
          '*'
        );
      }
    }
    this[eventName] = true;
  }
}

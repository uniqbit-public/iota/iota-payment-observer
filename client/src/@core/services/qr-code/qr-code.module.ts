import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { QrCodeComponent } from './qr-code.component';
import { QrCodeDirective } from './qr-code.directive';

const MODULES = [CommonModule];

const COMPONENTS = [QrCodeComponent];

const DIRECTIVES = [QrCodeDirective];

@NgModule({
  declarations: [...COMPONENTS, ...DIRECTIVES],
  imports: [...MODULES],
  exports: [...COMPONENTS, ...DIRECTIVES]
})
export class QrCodeModule {}

import { Injectable } from '@angular/core';
import {
  Router,
  ActivatedRoute,
  NavigationStart,
  NavigationExtras
} from '@angular/router';

@Injectable()
export class RoutingService {
  private lastpath: string;
  private currpath: string;

  data: any;

  constructor(
    private readonly router: Router,
    private readonly actRoute: ActivatedRoute
  ) {
    // tslint:disable-next-line: deprecation
    this.router.events.subscribe((observer) => {
      if (observer instanceof NavigationStart) {
        this.lastpath = this.router.url;
        this.currpath = observer.url;
      }
    });
  }

  async navigate(args: string[], extras?: NavigationExtras): Promise<boolean> {
    return this.router.navigate(args, extras);
  }

  getQueryParams() {
    return this.actRoute.snapshot.queryParams;
  }

  getUrlParams() {
    // does return undefined when actRoute is not injected in component
    return this.actRoute.snapshot.params;
  }

  getLastPath(): string {
    return this.lastpath;
  }

  getCurrPath(): string {
    if (this.currpath == undefined) {
      return;
    }
    return this.currpath.split('?')[0];
  }

  setCurrentPath(currentPath: string) {
    this.currpath = currentPath;
  }

  isRouteActive(route: string): boolean {
    const path = this.router.url;
    if (path) {
      const routeSplit = route.split('**');
      for (const r of routeSplit) {
        if (path.toLowerCase().indexOf(r.toLowerCase()) === -1) {
          return false;
        }
      }
      return true;
    }
    return false;
  }
}

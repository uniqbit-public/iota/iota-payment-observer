import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';

@Injectable()
export class LegalService {
  dataPolicyUrl: string;
  legalNoticeUrl: string;

  constructor(private readonly http: HttpClient) {}

  async fetchLegalLinks(): Promise<void> {
    if (this.dataPolicyUrl == undefined) {
      this.http
        .get(
          '/ipo/v1/legals/data'
        )
        .toPromise()
        .then((result: any) => {
          this.dataPolicyUrl = result.url;
        })
        .catch(err => console.error(err));
    }
    if (this.legalNoticeUrl == undefined) {
      this.http
        .get(
          '/ipo/v1/legals/notice'
        )
        .toPromise()
        .then((result: any) => {
          this.legalNoticeUrl = result.url;
        })
        .catch(err => console.error(err));
    }
  }
}

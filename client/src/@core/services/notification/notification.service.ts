import { Injectable } from '@angular/core';
import { ToastController } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';
import { Subject } from 'rxjs';

@Injectable()
export class NotificationService {
  // is triggered by notification.service;
  public readonly noteWasClosedSub: Subject<any> = new Subject();

  activeToasts: any[] = [];

  constructor(
    public readonly toastController: ToastController,
    private readonly translateService: TranslateService
  ) {}

  position: 'bottom' | 'top' | 'middle' = 'bottom';
  timeout = 5000;

  async showNotification(
    type: 'default' | 'info' | 'success' | 'warning' | 'error',
    title: string,
    body: string,
    noTranslation?: boolean,
    untranslatedBodySuffix?: string
  ) {
    let message: string;
    if (body && noTranslation) {
      message = body;
    } else if (body) {
      message =
        this.translateService.instant(body) +
        (untranslatedBodySuffix ? untranslatedBodySuffix : '');
    } else {
      message = this.translateService.instant(title);
    }

    let header: string;
    if (!body && title) {
      message = this.translateService.instant(title);
    } else if (title) {
      header = this.translateService.instant(title);
    }

    let color: string;
    switch (type.toLowerCase()) {
      case 'default':
        color = 'dark';
        break;
      case 'info':
        color = 'primary';
        break;
      case 'success':
        color = 'success';
        break;
      case 'warning':
        color = 'warning';
        break;
      case 'error':
        color = 'danger';
        break;
      default:
        color = 'dark';
    }

    // remove previous toast to not overlay each other
    for (const entry of this.activeToasts) {
      entry.dismiss();
    }
    this.activeToasts = [];

    const options = {
      header,
      message,
      position: this.position,
      color,
      duration: this.timeout,
      cssClass: 'toaster',
    };

    const toast = await this.toastController.create({
      ...options,
      buttons: [
        {
          side: 'end',
          icon: 'close',
          handler: () => {
            this.noteWasClosedSub.next(options);
          }
        }
      ]
    });
    await toast.present();
    this.activeToasts.push(toast);
  }

  overrideTimeoutInMs(time: number) {
    this.timeout = time;
  }

  resetTimeoutDelayed() {
    setTimeout(() => {
      this.timeout = 5000;
    }, 25);
  }
}

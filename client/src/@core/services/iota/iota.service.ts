import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { WindowEventService } from '../window-event/window-event.service';

@Injectable()
export class IOTAService {
  recieverAddress: any;
  data: string;
  isClosing = false;

  currentIOTAPrice: number;

  constructor(
    private readonly http: HttpClient,
    private readonly windowEventService: WindowEventService
  ) {}

  windowClose() {
    this.isClosing = true;
    if (
      this.recieverAddress == undefined ||
      this.recieverAddress.address == undefined
    ) {
      // don't notify parent window
      return;
    }

    this.windowEventService.emitMessage('closing', {
      address: this.recieverAddress.address,
      isSuccess: this.recieverAddress.isSuccess,
      isCancel: this.recieverAddress.isCancel
    });

    if (this.recieverAddress.isSuccess === true) {
      // don't send cancel to api
      return;
    }

    this.windowEventService.emitMessage('cancel', this.recieverAddress);

    this.http
      .patch(
        '/ipo/v1/addresses/cancel/' + this.recieverAddress.address,
        { isCancel: true },
        {}
      )
      .toPromise()
      .catch(err => console.error(err));
  }

  async getNewAddress(data?: any): Promise<any> {
    return new Promise((resolve, reject) => {
      let params = new HttpParams();
      if (!(data == undefined) && data.length) {
        let sanitizedData = data;
        if (typeof sanitizedData === 'object') {
          sanitizedData = window.btoa(JSON.stringify(sanitizedData));
        } else if (typeof sanitizedData === 'string') {
          sanitizedData = window.btoa(sanitizedData);
        }
        params = params.set('data', sanitizedData);
        if (typeof data === 'string') {
          try {
            this.data = JSON.parse(data);
          } catch (e) {
            console.error('data format is invalid', e);
            this.data = data;
          }
        } else {
          this.data = data;
        }
      }
      this.http
        .get('/ipo/v1/addresses', {
          params
        })
        .toPromise()
        .then(result => {
          this.recieverAddress = result;
          resolve(result);
        })
        .catch(err => reject(err));
    });
  }

  async getAddressBalance(address: string): Promise<any> {
    return new Promise((resolve, reject) => {
      this.http
        .get('/ipo/v1/addresses/' + address, {})
        .toPromise()
        .then(result => resolve(result))
        .catch(err => reject(err));
    });
  }

  async getCurrentIOTAPrice(symbol: string): Promise<any> {
    return new Promise(async (resolve, reject) => {
      if (symbol == undefined) {
        return resolve(undefined);
      }
      const queryParams = new HttpParams({
        fromObject: {
          symbol
        }
      });
      await this.http
        .get('/ipo/v1/prices', {
          params: queryParams
        })
        .toPromise()
        .then((result) => {
          resolve(result);
        })
        .catch((err) => reject(err));
    });
  }
}

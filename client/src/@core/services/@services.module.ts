import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { QrCodeModule } from '../../@core/services/qr-code/qr-code.module';
import { RoutingService } from './routing/routing.service';
import { ThemeService } from './theme/theme.service';
import { LanguageService } from './language/language.service';
import { IOTAService } from './iota/iota.service';
import { NotificationService } from './notification/notification.service';
import { LegalService } from './legal/legal.service';
import { WindowEventService } from './window-event/window-event.service';

const MODULES = [
  CommonModule,
  TranslateModule,
  QrCodeModule,
];

const SERVICES = [
  RoutingService,
  ThemeService,
  LanguageService,
  IOTAService,
  NotificationService,
  LegalService,
  WindowEventService,
];

@NgModule({
  declarations: [],
  entryComponents: [],
  imports: [...MODULES],
  providers: [...SERVICES]
})
export class ServicesModule {
  static forRoot(): ModuleWithProviders<ServicesModule> {
    return {
      ngModule: ServicesModule,
      providers: [...SERVICES]
    };
  }
}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { ServicesModule } from './services/@services.module';

export const MODULES = [
  CommonModule,
  HttpClientModule,
  ServicesModule,
];

const DIRECTIVES = [
];

@NgModule({
  imports: [...MODULES],
  declarations: [...DIRECTIVES],
  entryComponents: [],
  exports: []
})
export class CoreModule {
}

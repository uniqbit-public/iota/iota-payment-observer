#!/bin/bash

if [ -z "$ENV" ]; then
  export ENV=PROD
fi

if [ -z "$COLLECTOR_ADDRESS" ]; then
  echo "you need to provide a COLLECTOR_ADDRESS"
  exit 1
fi

if [ -z "$POSTGRES_HOST" ]; then
  export POSTGRES_HOST=ipodb
fi
if [ -z "$POSTGRES_USER" ]; then
  export POSTGRES_USER=ipo
fi
if [ -z "$POSTGRES_DB" ]; then
  export POSTGRES_DB=ipo
fi
if [ -z "$POSTGRES_PORT" ]; then
  export POSTGRES_PORT=5432
fi

if [ -z "$POSTGRES_PASSWORD" ]; then
  echo "you need to provide a production level postgresql password"
  exit 1
fi

if [ -z "$SEED_ENCRYPTION_PW" ]; then
  echo "you need to provide a production level seed encryption password"
  exit 1
fi

if [ -z "$IOTA_NETWORK" ]; then
  export IOTA_NETWORK="testnet"
fi

npm run prod

# What is this?
The `IOTA Payment Observer (IPO)`!
This project does implement a nest.js api with the iota.js package and an angular frontend to give for example E-Commerce shop developers an easy to integrate PayPal like payment flow. How does it operate? It does create seeds on the fly and packages dynamically created recipient addresses with given data packages, since a value transaction
in the IOTA Firefly wallet does not permit the use of usage notices. It then notifies given webhook address during the transaction lifecycle as well as emitting window events
for your own frontend to listen to.


# DEMOS
- Testnet one of the first iterations: https://www.youtube.com/watch?v=0vEopFL69iM
- Mainnet production implementation: https://www.youtube.com/watch?v=D2uj43u1bGw


# Known examples of productive implementations
- https://atlas-commander.com/


# Environment variables
you can customize endpoints and behaviour with following environment variables
```shell
# The IOTA Payment Observer does store on the fly created wallets/seed and 
# sessions in a PostgreSQL database; you might want to deploy your own.
# A default psql container like this one is sufficient: https://hub.docker.com/_/postgres

# MANDATORY VARS:
POSTGRES_PASSWORD=
SEED_ENCRYPTION_PW=     # password to encrypt your wallet seeds in the postgre database
COLLECTOR_ADDRESS=      # your main IOTA wallet recipient address, for the token collection; make sure it is an "atoi..." address for the testnet or an "iota..." address for the mainnet

# OPTIONAL BUT RECOMMENDED VARS:
ENV=DEV                 # when you use the docker container, it will default to PROD; see package.json scripts and Dockerfile entrypoint.sh
POSTGRES_HOST=          # target url/address of your psql sibling container
POSTGRES_PORT=          # port of your psql sibling container
POSTGRES_USER=      
POSTGRES_DB=ipodb       # db, defaults to ipodb
TX_COMPLETE_WEBHOOK=http://localhost:4040/ipo/v1/test/complete                  # does point to a dummy endpoint per default; should be an API route of your own
TX_DATA_VALIDATION_WEBHOOK=http://localhost:4040/ipo/v1/test/datavalidation     # does point to a dummy endpoint per default; should be an API route of your own


# OPTIONAL VARS:
WEBHOOK_SECRET=         # can be a token or a password which will be handed over for transaction completion; e. g. to authenticate on your applications' API
IOTA_NETWORK=testnet    # on which network should the IOTA Payment Observer operate; Development is testnet; it does default to testnet if not provided
LOG_LEVEL=error,warn,info,debug
TX_START_WEBHOOK=http://localhost:4040/ipo/v1/test/start          # does point to a dummy endpoint per default; should be an API route of your own; gets omitted when empty
TX_CANCELED_WEBHOOK=http://localhost:4040/ipo/v1/test/canceled    # does point to a dummy endpoint per default; should be an API route of your own; gets omitted when empty
TX_PARTIAL_WEBHOOK=http://localhost:4040/ipo/v1/test/partial      # does point to a dummy endpoint per default; should be an API route of your own; gets omitted when empty
TX_REPORT_WEBHOOK=http://localhost:4040/ipo/v1/test/report        # does point to a dummy endpoint per default; should be an API route of your own; gets omitted when empty
DATA_POLICY_URL="http://localhost:4200/ipo/#/legal/data"          # does point to a dummy data policy page; should point to your own
LEGAL_NOTICE_URL="http://localhost:4200/ipo/#/legal/notice"       # does point to a dummy legal notice page; should point to your own

```



# Test docker container locally
```shell
# start psql db
sh postgres/launch.sh
docker rm -f ipo
docker run --name ipo -e POSTGRES_PASSWORD=development -e SEED_ENCRYPTION_PW=development -d impulse87/iota-payment-observer:latest-dev


# [optional] check logs
docker logs -f ipo


# [optional] get shell into container
docker exec -it ipo /bin/bash


# [optional] prune old images
docker image prune -af

```



# Client test addresses
- with theme, language and example data
  - http://localhost:4100/ipo/#/transaction?data=%7B%22value%22:2000000%7D&theme=light&lang=de
  - http://localhost:4100/ipo/#/transaction?data=%7B%22value%22:2000000%7D&theme=dark&lang=de




# Button Templates, Code Snippets for integration
Example code snipped to integrate the IOTA Payment Observer button
- html
```html
<!-- dark -->
<button class="iota-btn" (click)="onIotaTx($event)">
  <img src="/ipo/assets/logos/iota-white(256px).png" alt="iota-transaction">
</button>

<!-- light -->
<button class="iota-btn" (click)="onIotaTx($event)">
  <img src="/ipo/assets/logos/iota-black(256px).png" alt="iota-transaction">
</button>
```
- scss
```scss
// dark
.iota-btn {
  background-color: #121212;
  border-radius: 10px;
  padding: 10px 30px;
  margin: 10px 0;

  .iota-logo {
    height: 30px;
  }

  &:hover {
    background-color: #303032;
  }
}

// light
.iota-btn {
  background-color: #d7d7de;
  border-radius: 10px;
  padding: 10px 30px;
  margin: 10px 0;

  .iota-logo {
    height: 30px;
  }

  &:hover {
    background-color: #efefef;
  }
}
```
- typescript
```typescript
onIotaTx(event) {
  const winId = 'ipo'; // put in whatever you want
  const port = window.location.hostname === 'localhost' || window.location.hostname === '127.0.0.1' ? ':4100' : '';
  const baseUrl = `${window.location.protocol}//${window.location.hostname}${port}`;
  const url = `${baseUrl}/ipo/#/load?id=${winId}&data={"value":2000000}&theme=dark&lang=en&symbol=USD`; // here you'd put your query params
  const options = `
    scrollbars=yes,
    width=500,
    height=650,
    left=${window.innerWidth / 2 - 250},
    top=${window.innerHeight / 2 - 325}
  `;
  const ipoWindow = window.open(url, winId, options);
  const onBackdrop = () => {
    const elem = document.getElementById('ipo-backdrop');
    if (!(elem == undefined)) {
      const parent = elem.parentElement;
      parent.removeChild(elem);
    }
    ipoWindow.postMessage('close', '*');
    // OPTIONAL: here goes your hook with a little delay, to check your own backend (webhook targets)
    // if transaction was reported as successful or canceled
  };
  const ipoWindowCheck = setInterval(() => {
    if (ipoWindow == undefined || ipoWindow.closed) {
      clearInterval(ipoWindowCheck);
      if (!(ipoWindow == undefined)) {
        onBackdrop();
      }
    }
  }, 250);
  // react to IPO window events
  let isSuccess = false;
  let isClosing = false;
  const handleIpoChildMessage = (event: any) => {
    switch (event.data.type) {
      case 'success':

        // TODO; to implement
        console.info('is success');

        isSuccess = true;
        break;
      case 'cancel':

        // TODO; to implement
        console.info('is cancel');

        isClosing = true;
        break;
      case 'closing':

        // TODO; to implement
        console.info('is closing');

        if (isSuccess) {
          // maybe repeat success feedback to user?
        }
        isClosing = true;
        break;
    }
    if (isClosing) {
      if (!(event.data.type == undefined)) {
        setTimeout(() => {
          window.removeEventListener('message', handleIpoChildMessage, false);
        }, 250);
      }
    }
  };
  window.addEventListener('message', handleIpoChildMessage, false);
  // backdrop div
  const backdrop = document.createElement('div');
  backdrop.setAttribute('id', 'ipo-backdrop');
  backdrop.setAttribute('style', 'position:absolute;top:0;bottom:0;left:0;right:0;background:rgba(0,0,0,0.8);z-index:9999999;');
  backdrop.addEventListener('click', () => onBackdrop);
  // info-wrapper div
  const info = document.createElement('div');
  info.setAttribute('style', 'position:absolute;top:50%;left:50%;transform:translate(-50%,-50%);color:#fff;text-align:center;');
  backdrop.appendChild(info);
  // iota-logo
  const logo = document.createElement('img');
  logo.setAttribute('src', `${baseUrl}/ipo/assets/logos/iota-white(256px).png`);
  logo.setAttribute('style', 'padding-bottom:25px;');
  info.appendChild(logo);
  // description
  const desc = document.createElement('div');
  desc.textContent = 'Don\'t see the IOTA Payment Observer window? Simply click here to close this backdrop and restart the process.';
  desc.setAttribute('style', 'font-size:14px;max-width:250px;margin:auto;');
  info.appendChild(desc);
  // attach to main window body
  const body = document.getElementsByTagName('body')[0];
  body.appendChild(backdrop);

}

```


# Example: tell the IPO window that the transaction was successful
- this will forward the window state to successful
```typescript
// ipoWindow is the reference the the previously opened window.open()
ipoWindow.postMessage('done', '*');
```
- if you want to close it automatically afterwards
```typescript
ipoWindow.postMessage('close', '*');
// or
ipoWindow.close();
```



# Models
- API Routes: https://drive.google.com/file/d/1M142yQ25JdOjhOlS3SqHUE7xkSUT_rOx/view?usp=sharing
- DB Entities: https://drive.google.com/file/d/1ziSiw4mQs5MCCsYjXtXS0t0dsOxYmzzw/view?usp=sharing



# Development references
- get testnet tokens: https://faucet.testnet.chrysalis2.com/
- https://www.npmjs.com/package/@iota/client
- https://client-lib.docs.iota.org/libraries/nodejs/examples.html


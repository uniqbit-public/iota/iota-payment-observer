FROM node:16-buster-slim as ipo

WORKDIR /var/api
COPY entrypoint.sh .
COPY package*.json .
COPY .env.prod .

RUN apt-get update \
  && apt-get install -y curl \
  && npm i --no-dev --no-audit --silent

COPY dist /var/api/dist/
COPY client/www /var/api/client/www/

CMD ["sh", "entrypoint.sh"]
